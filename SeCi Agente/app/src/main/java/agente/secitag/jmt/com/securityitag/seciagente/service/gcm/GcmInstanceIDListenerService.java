package agente.secitag.jmt.com.securityitag.seciagente.service.gcm;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class GcmInstanceIDListenerService extends InstanceIDListenerService {
    @Override
    public void onTokenRefresh() {
        System.out.println("onTokenRefresh()()()()");
        Intent intent = new Intent(this, GcmRegistrationIntentService.class);
        startService(intent);
    }
}