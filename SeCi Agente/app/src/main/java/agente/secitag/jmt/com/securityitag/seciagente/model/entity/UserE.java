package agente.secitag.jmt.com.securityitag.seciagente.model.entity;

import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by Lester on 11/20/15.
 */
public class UserE extends SugarRecord<UserE> implements Serializable {
    @Expose
    String first_name;
    @Expose
    String last_name;
    @Expose
    String id_number;

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }
}
