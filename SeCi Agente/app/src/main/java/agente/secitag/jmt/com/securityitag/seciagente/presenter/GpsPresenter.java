package agente.secitag.jmt.com.securityitag.seciagente.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import agente.secitag.jmt.com.securityitag.seciagente.model.API;
import agente.secitag.jmt.com.securityitag.seciagente.model.S;
import agente.secitag.jmt.com.securityitag.seciagente.model.entity.LocationE;
import agente.secitag.jmt.com.securityitag.seciagente.service.gps.LocationFusedService;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.GeolocationView;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.GpsView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class GpsPresenter {
    GpsView delegate;
    GeolocationView geoDelegate;
    private String url;
    private Gson gson;
    private Context ctx;
    private BroadcastReceiver mGpsBroadcastReceiver;
    private Intent gpsService;

    public GpsPresenter(GpsView delegate){
        this.delegate = delegate;
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
        ctx = delegate.getContext();
        gpsService = new Intent(ctx,LocationFusedService.class);
    }
    public GpsPresenter(GeolocationView geoDelegate){
        this.geoDelegate = geoDelegate;
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
        ctx = this.geoDelegate.getContext();
    }

    public LatLng getLastPosition(){
        LatLng lastPos= new LatLng(-12.055342,-77.0802049);
        List<LocationE> locs = LocationE.listAll(LocationE.class);
        int sz = locs.size();
        if(sz>0)lastPos = locs.get(sz-1).getLatLng();
        System.out.println("LastPos:"+lastPos.latitude+","+lastPos.longitude);
        return lastPos ;
    }

    public void startGpsService(){
        ctx.startService(gpsService);
        cleanLocations();
    }

    private void cleanLocations(){
        List<LocationE> locations = LocationE.listAll(LocationE.class);
        if(locations.size()>0){
            LocationE loc = locations.get(locations.size()-1);
            LocationE.deleteAll(LocationE.class);
            loc.save();
        }
    }
    public void startConnection(){
        mGpsBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                double lat = intent.getExtras().getDouble(S.POSITION.latitude);
                double lng = intent.getExtras().getDouble(S.POSITION.longitude);
                System.out.println("Nueva posicion encontrada:"+lat+","+lng);
                delegate.updateLatLng(new LatLng(lat,lng));
            }
        };
        LocalBroadcastManager.getInstance(ctx).registerReceiver(mGpsBroadcastReceiver,new IntentFilter(LocationFusedService.NEW_GPS));
    }

    public void stopConnection(){
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(mGpsBroadcastReceiver);
    }
    public void stopGpsService(){
        ctx.stopService(gpsService);
    }

    public void sendGpsLocation(double lat,double lng){
        url = S.VAR.base_url + "/api/user";
        Map<String,String> params = new HashMap<>();
        params.put("lat",String.valueOf(lat));
        params.put("lng",String.valueOf(lng));
        new API(ctx).put(url, params);
    }
}