package agente.secitag.jmt.com.securityitag.seciagente.service.gps;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.Date;

import agente.secitag.jmt.com.securityitag.seciagente.model.S;
import agente.secitag.jmt.com.securityitag.seciagente.model.entity.LocationE;
import agente.secitag.jmt.com.securityitag.seciagente.presenter.GpsPresenter;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.GeolocationView;

/**
 * Created by JMTech-Android on 09/06/2015.
 */
public class LocationFusedService extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener, GeolocationView {
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 60*1000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation;
    protected GpsPresenter gpsPresenter;
    public final static String NEW_GPS = "NEW_GPS";
    protected Boolean mRequestingLocationUpdates;
    Intent intent;

    @Override
    public void onCreate() {
        super.onCreate();
        mRequestingLocationUpdates = false;
        gpsPresenter = new GpsPresenter(this);
        intent = new Intent();
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        System.out.println("geoAAAAAAAAAAAAA");
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("geoBBBBBBBBBBB");
        System.out.println("creo que va a necesitar Iniciar geolocalizacion...");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mGoogleApiClient.isConnected()) {
                    System.out.println("Iniciar geolocalizacion...");
                    startLocationUpdates();
                    mRequestingLocationUpdates = true;
                }else
                    System.out.println("No se inicio geolocalizacion...");
            }
        }, 1500);
        return super.onStartCommand(intent, flags, startId);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        intent.putExtra(S.POSITION.latitude,location.getLatitude());
        intent.putExtra(S.POSITION.longitude,location.getLongitude());
        intent.setAction(NEW_GPS);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        LocationE locationE = new LocationE();
        locationE.setLat(location.getLatitude());
        locationE.setLng(location.getLongitude());
        locationE.setTime(new Date().getTime());
        locationE.save();
        gpsPresenter.sendGpsLocation(location.getLatitude(),location.getLongitude());
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onDestroy() {
        System.out.println("Finalizando");
        super.onDestroy();
        if (mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }
}