package agente.secitag.jmt.com.securityitag.seciagente.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import agente.secitag.jmt.com.securityitag.seciagente.R;
import agente.secitag.jmt.com.securityitag.seciagente.presenter.LoginPresenter;
import agente.secitag.jmt.com.securityitag.seciagente.view.fragment.LoginFragment;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.LoginView;

/**
 * Created by JMTech-Android on 30/10/2015.
 */
public class AccessPanelSeciActivity extends AppCompatActivity implements LoginFragment.OnLoginListener,LoginView{
    private final LoginFragment loginFragment = LoginFragment.instance();
    private LoginPresenter loginPresenter;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_panel);
        loginPresenter = new LoginPresenter(this);
        showMapHome();
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    private void showMapHome() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, loginFragment).commit();
    }


    @Override
    public void onLoginStarted() {

    }
    @Override
    public void onLoginStopped() {

    }
    @Override
    public void login(String user, String pass) {
        loginPresenter.login(user,pass);
    }

    @Override
    public void onRequestSuccess(Object object) {
        startActivity(new Intent(this,HomeSeciActivity.class));
        finish();
    }
    @Override
    public void onRequestError(Object object) {
        loginFragment.showError(object.toString());
        loginFragment.enableButtonLogin(true);
    }
    @Override
    public Context getContext() {
        return this;
    }
}