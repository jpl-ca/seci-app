package agente.secitag.jmt.com.securityitag.seciagente.presenter;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import agente.secitag.jmt.com.securityitag.seciagente.model.API;
import agente.secitag.jmt.com.securityitag.seciagente.model.S;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.ApiRequest;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.MapAlertView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class MapAlertPresenter {
    MapAlertView delegate;
    private String url;
    private Gson gson;
    private Context ctx;

    public MapAlertPresenter(MapAlertView delegate){
        this.delegate = delegate;
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
        ctx = delegate.getContext();
    }

    public void incidentAlertAccept(final int alert_id){
        url = S.VAR.base_url + "/api/incident-alert/"+alert_id;
        Map<String,String> params = new HashMap<>();
        new API(ctx,new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                System.out.println("---->"+response.toString());
                delegate.onRequestIncidentSuccess(S.ALERTA.ATENCION_CORRECTO);
            }
            @Override
            public void onRequestError (Object error){
                try {
                    System.out.println("AAA->"+error.toString());
                    JSONObject jo = new JSONObject(error.toString());
                    String m = jo.getString("message");
                    System.out.println("M ES:"+m);
                    delegate.onRequestError(S.ALERTA.ERROR_ALERTA_FUE_ATENDIDO_PREVIAMENTE,m);
                } catch (JSONException e) {}
            }

            @Override
            public void onRequestNull() {
//                incidentAlertAccept(alert_id);
            }
        }).put(url, params);
    }

    public void incidentAlertFinish(final int alert_id, final String comment) {
        url = S.VAR.base_url + "/api/incident-alert/"+alert_id;
        Map<String,String> params = new HashMap<>();
        params.put("agent_review",comment);
        new API(ctx,new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                System.out.println("====>"+response.toString());
                delegate.onRequestIncidentSuccess(S.ALERTA.FINALIZACION_CORRECTO);
            }
            @Override
            public void onRequestError (Object error){
                try {
                    JSONObject jo = new JSONObject(error.toString());
                    String m = jo.getString("message");
                    delegate.onRequestError(S.ALERTA.ERROR_NO_FINALIZADO,m);
                } catch (JSONException e) {}
            }

            @Override
            public void onRequestNull() {
//                incidentAlertFinish(alert_id,comment);
            }
        }).put(url, params);
    }
}