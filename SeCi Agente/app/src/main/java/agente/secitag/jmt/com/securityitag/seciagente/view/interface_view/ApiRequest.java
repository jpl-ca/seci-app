package agente.secitag.jmt.com.securityitag.seciagente.view.interface_view;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface ApiRequest {
    void onRequestSuccess(Object object);
    void onRequestError(Object object);
    void onRequestNull();
}