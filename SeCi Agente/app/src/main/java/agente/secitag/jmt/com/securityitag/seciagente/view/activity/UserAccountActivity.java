package agente.secitag.jmt.com.securityitag.seciagente.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import agente.secitag.jmt.com.securityitag.seciagente.R;
import agente.secitag.jmt.com.securityitag.seciagente.model.entity.UserE;
import agente.secitag.jmt.com.securityitag.seciagente.presenter.UserAccessPresenter;
import agente.secitag.jmt.com.securityitag.seciagente.view.fragment.UserAccountFragment;

/**
 * Created by JMTech-Android on 30/10/2015.
 */
public class UserAccountActivity extends AppCompatActivity implements UserAccountFragment.OnUserAccountListener{
    private final UserAccountFragment userFragment = UserAccountFragment.instance();
    private UserAccessPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_panel);
        presenter = new UserAccessPresenter();
        showPanel();
    }

    private void showPanel() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, userFragment).commit();
    }

    @Override
    public UserE getUserData() {
        UserE u = presenter.getUserInfo();
        return u;
    }
}