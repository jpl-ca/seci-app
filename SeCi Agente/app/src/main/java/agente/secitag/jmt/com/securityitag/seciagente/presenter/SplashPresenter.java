package agente.secitag.jmt.com.securityitag.seciagente.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import agente.secitag.jmt.com.securityitag.seciagente.model.API;
import agente.secitag.jmt.com.securityitag.seciagente.model.S;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.ApiRequest;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.SplashView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class SplashPresenter {
    SplashView splashView;
    private String url;
    private Gson gson;
    public SplashPresenter(SplashView splashView){
        this.splashView = splashView;
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }
    public void getInfo(){
        url = S.VAR.base_url + "/api/user";
        new API(splashView.getContext(),new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                System.out.println(response.toString());
                splashView.onRequestSuccess(response);
            }
            @Override
            public void onRequestError(Object error) {
                splashView.onRequestError(error);
            }
            @Override
            public void onRequestNull() {
//                getInfo();
            }
        }).get(url);
    }
}