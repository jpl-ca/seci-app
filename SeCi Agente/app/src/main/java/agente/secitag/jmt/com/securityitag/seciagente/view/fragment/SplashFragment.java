package agente.secitag.jmt.com.securityitag.seciagente.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import agente.secitag.jmt.com.securityitag.seciagente.R;


/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class SplashFragment extends Fragment {
    private OnSplashListener presenter;

    public static SplashFragment instance()
    {
        return new SplashFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash, container, false);
        return rootView;
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        if (activity instanceof OnSplashListener) {
            this.presenter = (OnSplashListener) activity;
        } else {
            throw new ClassCastException("debe implementar OnDashboardListener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        this.presenter.onSplashStarted();
    }

    @Override
    public void onStop(){
        super.onStop();
        this.presenter.onSplashStopped();
    }

    public interface OnSplashListener{
        void onSplashStarted();
        void onSplashStopped();
    }
}