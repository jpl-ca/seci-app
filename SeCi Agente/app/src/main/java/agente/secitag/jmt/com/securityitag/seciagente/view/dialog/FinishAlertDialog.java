package agente.secitag.jmt.com.securityitag.seciagente.view.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import agente.secitag.jmt.com.securityitag.seciagente.R;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.CallbackAlertDialog;

/**
 * Created by JMTech-Android on 19/10/2015.
 */
public class FinishAlertDialog {
    private AlertDialog.Builder builder;
    private Context ctx;
    public FinishAlertDialog(Context ctx){
        this.ctx = ctx;
        builder = new AlertDialog.Builder(ctx, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(ctx.getString(R.string.finalizar_alerta));
    }
    public void show(final CallbackAlertDialog _cb){
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_alert, null, false);
        final EditText txtCom = (EditText)view.findViewById(R.id.txt_comentario);
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE: _cb.finish(txtCom.getText().toString()); break;
                    case DialogInterface.BUTTON_NEGATIVE: break;
                }
            }
        };
        builder.setView(view);
        builder.setNegativeButton(ctx.getString(R.string.cancelar), dialogClickListener);
        builder.setPositiveButton(ctx.getString(R.string.finalizar), dialogClickListener);
        builder.show();
    }
}