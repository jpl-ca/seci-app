package agente.secitag.jmt.com.securityitag.seciagente.view.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import agente.secitag.jmt.com.securityitag.seciagente.R;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.CallbackDialog;

/**
 * Created by JMTech-Android on 19/10/2015.
 */
public class AlertaRoboDialog {
    private AlertDialog.Builder builder;
    private Context ctx;
    public AlertaRoboDialog(Context ctx){
        this.ctx = ctx;
        builder = new AlertDialog.Builder(ctx, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(ctx.getString(R.string.nueva_alerta));
    }
    public void show(int alert_id,String mensaje,final CallbackDialog _cb){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE: _cb.finish(); break;
                    case DialogInterface.BUTTON_NEGATIVE: break;
                }
            }
        };
        mensaje +="\n\n" + "¿Va atender la alerta del ciudadano?";
        builder.setMessage(mensaje);
        builder.setNegativeButton(ctx.getString(R.string.cancelar), dialogClickListener);
        builder.setPositiveButton(ctx.getString(R.string.atender), dialogClickListener);
        builder.setCancelable(false);
        builder.show();
    }
}