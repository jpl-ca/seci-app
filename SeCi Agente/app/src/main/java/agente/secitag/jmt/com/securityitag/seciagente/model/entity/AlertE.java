package agente.secitag.jmt.com.securityitag.seciagente.model.entity;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;
import java.io.Serializable;

/**
 * Created by Lester on 11/20/15.
 */
public class AlertE extends SugarRecord<AlertE> implements Serializable {
    @Expose
    int alert_id;
    @Expose
    String usuario;
    @Expose
    String mensaje;
    @Expose
    String direccion;
    @Expose
    double latitude;
    @Expose
    double longitude;

    public int getAlert_id() {
        return alert_id;
    }

    public void setAlert_id(int alert_id) {
        this.alert_id = alert_id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public LatLng getPosicion() {
        return new LatLng(latitude,longitude);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(LatLng ll) {
        latitude = ll.latitude;
        longitude = ll.longitude;
    }
}