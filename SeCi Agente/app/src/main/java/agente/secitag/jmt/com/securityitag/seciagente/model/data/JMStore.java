package agente.secitag.jmt.com.securityitag.seciagente.model.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by JMTech-Android on 19/10/2015.
 */
public class JMStore {
    Context ctx;
    SharedPreferences DB;
    private String PREF_TAG_IS_CONNECTED = "IS_TAG_CONNECTED";
    private String PREF_IS_SERVICE_RUNNING = "IS_SERVICE_STARTED";
    private String PREF_MAC_BT = "PREF_MAC_BLUETOOTH";
    public JMStore(Context ctx){
        DB= ctx.getSharedPreferences("sp_seci_jm",Context.MODE_PRIVATE);
    }
    public void putString(String key,String value) {
        System.out.println("Guardando:"+value);
        SharedPreferences.Editor editor = DB.edit();
        editor.putString(key,value);
        editor.apply();
    }
    public void removeString(String key) {
        SharedPreferences.Editor editor = DB.edit();
        editor.remove(key);
        editor.apply();
    }
    public String getString(String key) {
        if(DB.contains(key)) {
            return DB.getString(key, "");
        }
        return "";
    }



    public void putBoolean(String key,boolean value) {
        System.out.println("Guardando:"+value);
        SharedPreferences.Editor editor = DB.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }
    public boolean getBoolean(String key) {
        if(DB.contains(key)) {
            return DB.getBoolean(key, false);
        }
        return false;
    }
}