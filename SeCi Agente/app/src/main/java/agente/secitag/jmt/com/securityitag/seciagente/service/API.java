package agente.secitag.jmt.com.securityitag.seciagente.service;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import agente.secitag.jmt.com.securityitag.seciagente.model.S;
import agente.secitag.jmt.com.securityitag.seciagente.model.data.JMStore;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.ApiRequest;

/**
 * Created by JMTech-Android on 19/10/2015.
 */
public class API implements Response.Listener<String>,Response.ErrorListener{
    ApiRequest callback;
    Context ctx;
    private RequestQueue queue;
    private JMStore jmStore;
    private Map<String, String> headers;
    private static API mInstance = null;

//    private API(){
//        headers = new HashMap<>();
//    }
//
//    public static API getInstance(){
////        if(mInstance == null)
//            mInstance = new API();
//        return mInstance;
//    }
//
//    public void get(String service_url){
//        System.out.println("http-get:"+service_url);
//        queue = Volley.newRequestQueue(ctx);
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, service_url,this, this){
//            @Override
//            public Map<String, String> getHeaders()throws AuthFailureError{
//                loadHeaderSaved(headers);
//                return headers;
//            }
//            @Override
//            protected VolleyError parseNetworkError(VolleyError volleyError){
//                if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
//                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
//                    volleyError = error;
//                }
//                return volleyError;
//            }
//        };
//        queue.add(stringRequest);
//    }
//    public void delete(String service_url){
//        System.out.println("http-delete:"+service_url);
//        queue = Volley.newRequestQueue(ctx);
//        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, service_url,this, this){
//            @Override
//            public Map<String, String> getHeaders()throws AuthFailureError{
//                loadHeaderSaved(headers);
//                return headers;
//            }
//            @Override
//            protected VolleyError parseNetworkError(VolleyError volleyError){
//                if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
//                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
//                    volleyError = error;
//                }
//                return volleyError;
//            }
//        };
//        queue.add(stringRequest);
//    }
//
//    public void post(String service_url,final Map<String,String> params){
//        System.out.println("http-post:"+service_url);
//        queue = Volley.newRequestQueue(ctx);
//        StringRequest stringRequest = new StringRequest(Request.Method.POST,service_url, this, this){
//            @Override
//            protected Map<String,String> getParams(){
//                for (String k : params.keySet()){
//                    System.out.println("PR***********"+k+" ->"+params.get(k));
//                }
//                return params;
//            }
//            @Override
//            public Map<String, String> getHeaders()throws AuthFailureError{
//                loadHeaderSaved(headers);
//                for (String k : headers.keySet()){
//                    System.out.println("HHH***********"+k+" ->"+headers.get(k));
//                }
//                return headers;
//            }
//
//            @Override
//            protected Response<String> parseNetworkResponse(NetworkResponse response) {
//                return super.parseNetworkResponse(response);
//            }
//            @Override
//            protected VolleyError parseNetworkError(VolleyError volleyError){
//                if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
//                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
//                    volleyError = error;
//                }
//                return volleyError;
//            }
//        };
//        queue.add(stringRequest);
//    }
//
//    public void put(String service_url,final Map<String,String> params){
//        System.out.println("http-put:"+service_url);
//        queue = Volley.newRequestQueue(ctx);
//        StringRequest stringRequest = new StringRequest(Request.Method.PUT,service_url, this, this){
//            @Override
//            protected Map<String,String> getParams(){
//                return params;
//            }
//            @Override
//            public Map<String, String> getHeaders()throws AuthFailureError{
//                loadHeaderSaved(headers);
//                return headers;
//            }
//            @Override
//            protected Response<String> parseNetworkResponse(NetworkResponse response) {
//                return super.parseNetworkResponse(response);
//            }
//            @Override
//            protected VolleyError parseNetworkError(VolleyError volleyError){
//                if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
//                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
//                    volleyError = error;
//                }
//                return volleyError;
//            }
//        };
//        queue.add(stringRequest);
//    }
//
//    private void loadHeaderSaved(Map<String, String> headers) {
//        headers.put(S.CLIENT.Authorization,jmStore.getString(S.CLIENT.token_type)+" "+jmStore.getString(S.CLIENT.access_token));
//        System.out.println("CK="+S.CLIENT.Authorization+":"+jmStore.getString(S.CLIENT.token_type)+" "+jmStore.getString(S.CLIENT.access_token));
//    }
//
//    public API setContext(Context _ctx){
//            ctx = _ctx;
//            jmStore = new JMStore(ctx);
//        return mInstance;
//    }
//    public API setCallbackk(ApiRequest _callback){
//            callback = _callback;
//        return mInstance;
//    }
//    public API setHeaders(Map<String, String> headers){
//        this.headers = headers;
//        return mInstance;
//    }
//
    @Override
    public void onResponse(String response) {
        if (callback!=null)
            callback.onRequestSuccess(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if(error.getMessage()==null){
            System.out.println("Retorno error NULL");
            callback.onRequestNull();
        }else{
            System.out.println("------------*****************");
            System.out.println("Error:::"+error.getMessage());
            if (callback!=null)
                callback.onRequestError(error.getMessage());
        }
    }
}