package agente.secitag.jmt.com.securityitag.seciagente.view.fragment;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import agente.secitag.jmt.com.securityitag.seciagente.R;
import agente.secitag.jmt.com.securityitag.seciagente.model.entity.AlertE;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class MapHomeFragment extends Fragment implements OnMapReadyCallback{
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.ll_root) protected LinearLayout ll_root;
    private LatLng ME;
    private GoogleMap gMap;
    private Marker mrkr,mrkrCitizen;
    private OnMapHomeListener presenter;
    private float MapZoom = 16;
    private CameraPosition cameraPosition;
    private Circle circleME;
    private ValueAnimator vAnimator;
    private boolean zoomEfect = false;
    private String markerCtzId;
    AlertE alertaActual;

    public static MapHomeFragment instance(){
        return new MapHomeFragment();
    }
    @Bind(R.id.fabFinishAlert) FloatingActionButton fab_finsish_alert;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map_home, container, false);
        ButterKnife.bind(this, rootView);
        fab_finsish_alert.setImageResource(R.mipmap.ic_pencil_white);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        toolbar.setLogo(R.mipmap.ic_logo_bar);
        toolbar.setTitle("  "+getString(R.string.s_agente));
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        return rootView;
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        if (activity instanceof OnMapHomeListener) {
            this.presenter = (OnMapHomeListener) activity;
        } else {
            throw new ClassCastException("debe implementar OnDashboardListener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        this.presenter.onHomeMapStarted();
    }

    @Override
    public void onStop(){
        super.onStop();
        this.presenter.onHomeMapStopped();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        gMap = map;
        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        CameraUpdate cam = CameraUpdateFactory.newLatLngZoom(presenter.getLastPosition(),8);
        gMap.moveCamera(cam);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                gMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        MapZoom = cameraPosition.zoom;
                    }
                });
                updateLatLng(presenter.getLastPosition());
            }
        }, 1500);
        List<AlertE> alerts = AlertE.listAll(AlertE.class);
        if(alerts.size()>0){
            showAlertInfo(alerts.get(0));
        }
    }

    public void updateLatLng(LatLng ll){
        ME = ll;
        if(mrkr==null) mrkr = gMap.addMarker(new MarkerOptions().position(ME).title(getString(R.string.s_mi_ubicacion)).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_agent)));
        else mrkr.setPosition(ME);
        cameraPosition = new CameraPosition.Builder().target(ME).zoom(MapZoom).build();
        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 5000, null);
    }

    public void showAlertInfo(AlertE alertaActual) {
        this.alertaActual = alertaActual;
        if(mrkrCitizen!=null)mrkrCitizen.remove();
        fab_finsish_alert.setVisibility(View.VISIBLE);
        mrkrCitizen = gMap.addMarker(new MarkerOptions().position(alertaActual.getPosicion()).title("Asalto robo silencioso").snippet(alertaActual.getDireccion()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_citizen)));
        markerCtzId = mrkrCitizen.getId();
    }

    @OnClick(R.id.fabFinishAlert)
    public void showFinishAlert(){
        if(alertaActual.getAlert_id()!=0)
            presenter.showFinishAlert(alertaActual.getAlert_id());
    }

    public void hideAlertInfo() {
        if(mrkrCitizen!=null)mrkrCitizen.remove();
        fab_finsish_alert.setVisibility(View.GONE);
        alertaActual = null;
    }

    public void showError(String mensaje_error) {
        Snackbar.make(ll_root, mensaje_error, Snackbar.LENGTH_LONG).show();
    }

    public interface OnMapHomeListener{
        void onHomeMapStarted();
        void onHomeMapStopped();
        void showFinishAlert(int alert_id);
        LatLng getLastPosition();
        void logout();
        void showProfile();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_map, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            presenter.logout();
            return true;
        }else if (id == R.id.action_profile) {
            presenter.showProfile();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}