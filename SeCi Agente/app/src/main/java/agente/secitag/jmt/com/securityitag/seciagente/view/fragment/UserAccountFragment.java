package agente.secitag.jmt.com.securityitag.seciagente.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import agente.secitag.jmt.com.securityitag.seciagente.R;
import agente.secitag.jmt.com.securityitag.seciagente.model.entity.UserE;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class UserAccountFragment extends Fragment {
    @Bind(R.id.txt_nombre_usuario) TextView txt_nombre_usuario;
    @Bind(R.id.txt_apellido_usuario) TextView txt_apellido_usuario;
    @Bind(R.id.txt_dni) TextView txt_dni;
    @Bind(R.id.ll_root) LinearLayout ll_root;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private UserE dataUser;
    private OnUserAccountListener presenter;

    public static UserAccountFragment instance(){
        return new UserAccountFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_account, container, false);
        ButterKnife.bind(this, rootView);
        toolbar.setTitle("  " + getString(R.string.s_cuenta));
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        fillData();
    }

    private void fillData() {
        dataUser = presenter.getUserData();
        if(dataUser !=null){
            txt_nombre_usuario.setText(dataUser.getFirst_name());
            txt_apellido_usuario.setText(dataUser.getLast_name());
            txt_dni.setText(dataUser.getId_number());
        }
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        if (activity instanceof OnUserAccountListener) {
            this.presenter = (OnUserAccountListener) activity;
        } else {
            throw new ClassCastException("debe implementar OnRegisterListener");
        }
    }

    public void showError(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public interface OnUserAccountListener{
        UserE getUserData();
    }
}