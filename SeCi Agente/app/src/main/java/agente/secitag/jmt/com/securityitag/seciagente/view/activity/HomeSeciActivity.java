package agente.secitag.jmt.com.securityitag.seciagente.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import com.google.android.gms.maps.model.LatLng;
import agente.secitag.jmt.com.securityitag.seciagente.R;
import agente.secitag.jmt.com.securityitag.seciagente.model.S;
import agente.secitag.jmt.com.securityitag.seciagente.model.entity.AlertE;
import agente.secitag.jmt.com.securityitag.seciagente.presenter.GcmPresenter;
import agente.secitag.jmt.com.securityitag.seciagente.presenter.GpsPresenter;
import agente.secitag.jmt.com.securityitag.seciagente.presenter.LogoutPresenter;
import agente.secitag.jmt.com.securityitag.seciagente.presenter.MapAlertPresenter;
import agente.secitag.jmt.com.securityitag.seciagente.view.dialog.AlertGpsDialog;
import agente.secitag.jmt.com.securityitag.seciagente.view.dialog.AlertaRoboDialog;
import agente.secitag.jmt.com.securityitag.seciagente.view.dialog.FinishAlertDialog;
import agente.secitag.jmt.com.securityitag.seciagente.view.fragment.MapHomeFragment;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.CallbackAlertDialog;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.CallbackDialog;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.GcmView;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.GpsView;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.LogoutView;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.MapAlertView;

/**
 * Created by JMTech-Android on 30/10/2015.
 */
public class HomeSeciActivity extends AppCompatActivity implements MapHomeFragment.OnMapHomeListener,GpsView,GcmView,MapAlertView,LogoutView{
    private GpsPresenter gpsPresenter;
    private GcmPresenter gcmPresenter;
    private LogoutPresenter logoutPresenter;
    private MapAlertPresenter mapAlertPresenter;
    private final MapHomeFragment mapHomeFragment = MapHomeFragment.instance();
    private int REQUEST_CODE = 10101;
    private AlertaRoboDialog alertNewIncident;
    private AlertE alerta;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        gpsPresenter = new GpsPresenter(this);
        gcmPresenter = new GcmPresenter(this);
        mapAlertPresenter = new MapAlertPresenter(this);
        logoutPresenter = new LogoutPresenter(this);
        showMapHome();
        startGeolocationGps();
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    private void showMapHome() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, mapHomeFragment).commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public Context getContext() {
        return this;
    }


    @Override
    public void onRequestIncidentSuccess(int type) {
        if(type == S.ALERTA.FINALIZACION_CORRECTO){//finalizado
            mapHomeFragment.hideAlertInfo();
            AlertE.deleteAll(AlertE.class);
        }else if(type == S.ALERTA.ATENCION_CORRECTO){
            mapHomeFragment.showAlertInfo(alerta);
            alerta.save();
            alertNewIncident = null;
        }
    }

    @Override
    public void onRequestError(int error_type,String mensaje_error) {
        if(error_type==S.ALERTA.ERROR_ALERTA_FUE_ATENDIDO_PREVIAMENTE){
//            String mensaje_error = "Alerta fue atendido por otro agemte";
            mapHomeFragment.showError(mensaje_error);
        }else if(error_type==S.ALERTA.ERROR_NO_FINALIZADO){
//            String mensaje_error = "Error de finalizacion, intente luego...!!!";
            mapHomeFragment.showError(mensaje_error);
        }
    }

    ////Pertenece a GPSView.
    @Override
    public void updateLatLng(LatLng ll) {
        mapHomeFragment.updateLatLng(ll);
    }

    // GPS ACTIVE
    private void startGeolocationGps() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if(!provider.contains("gps")){
            alertTurnGPS();
        }else{
            gpsPresenter.startGpsService();
        }
    }
    private void alertTurnGPS(){
        new AlertGpsDialog(this).show(new CallbackDialog() {
            @Override
            public void finish() {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_CODE && resultCode == 0){
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if(provider != null){
                startGeolocationGps();
            }else{
                alertTurnGPS();
            }
        }
    }

    @Override
    public void onHomeMapStarted() {
        gpsPresenter.startConnection();
        gcmPresenter.sendGcmId();
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            if(extras.getBoolean(S.GCM.from_push)){
                final int alert_id = extras.getInt(S.ALERTA.alert_id);
                String mensaje = extras.getString(S.GCM.message);
                System.out.println("------> El mensaje esssssss:"+mensaje);
                final String direccion = extras.getString(S.ALERTA.direccion);
                mensaje += "\n" + "Direccion: "+direccion;
                final LatLng ll = new LatLng(extras.getDouble(S.POSITION.latitude),extras.getDouble(S.POSITION.longitude));
                alerta = new AlertE();
                alerta.setAlert_id(alert_id);
                alerta.setDireccion(direccion);
                alerta.setMensaje(mensaje);
                alerta.setLatitude(ll);
                alertNewIncident = new AlertaRoboDialog(this);
                alertNewIncident.show(alert_id, mensaje, new CallbackDialog() {
                    @Override
                    public void finish() {
                        mapAlertPresenter.incidentAlertAccept(alerta.getAlert_id());
                    }
                });
                getIntent().putExtra(S.GCM.from_push,false);
            }
        }
    }
    @Override
    public void onHomeMapStopped() {
        gpsPresenter.stopConnection();
    }

    @Override
    public void showFinishAlert(final int alert_id) {
        new FinishAlertDialog(this).show(new CallbackAlertDialog() {
            @Override
            public void finish(String commnent) {
                mapAlertPresenter.incidentAlertFinish(alert_id,commnent);
            }
        });
    }

    @Override
    public LatLng getLastPosition() {
        return gpsPresenter.getLastPosition();
    }

    @Override
    public void logout() {
        logoutPresenter.logout();
        gpsPresenter.stopGpsService();
        Intent it = new Intent(this,AccessPanelSeciActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(it);
        finish();
    }

    @Override
    public void showProfile() {
        Intent it = new Intent(this,UserAccountActivity.class);
        startActivity(it);
    }
}