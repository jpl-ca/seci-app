package agente.secitag.jmt.com.securityitag.seciagente.service.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import agente.secitag.jmt.com.securityitag.seciagente.R;
import agente.secitag.jmt.com.securityitag.seciagente.model.S;
import agente.secitag.jmt.com.securityitag.seciagente.view.activity.HomeSeciActivity;
import agente.secitag.jmt.com.securityitag.seciagente.view.activity.SplashActivity;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class ListenerGcmService extends GcmListenerService {
    private int NOTIFICATION_NEWALERT = 101;
    @Override
    public void onMessageReceived(String from, Bundle data) {
        System.out.println("--------------------->");
        System.out.println("From es::::"+from);
        System.out.println(data.toString());
        if (from.startsWith("/topics/")) {
            System.out.println("message received from some topic.");
        } else {
            System.out.println("normal downstream message.");
        }
        try {
            int seci_notification_type = Integer.parseInt(data.getString("gcm.notification.seci_notification_type"));
            if(seci_notification_type==S.ALERTA.NUEVA_ALERTA){
                JSONObject citizen_=  new JSONObject(data.getString("citizen"));
                String ciudadano = citizen_.getString("first_name")+" "+citizen_.getString("last_name");
                String email = citizen_.getString("email");
                String titulo = "SeCi Alerta";
                String mensaje = data.getString("gcm.notification.message");
                String direccion = data.getString("address");
                String tag = "Alerta de robo";
                int alert_id = Integer.parseInt(data.getString("id"));
                double lat = Double.parseDouble(data.getString("lat"));
                double lng = Double.parseDouble(data.getString("lng"));
                sendNotification(titulo,mensaje,tag,alert_id,lat,lng,direccion);
            }else if(seci_notification_type==S.ALERTA.ALERTA_FUE_ATENDIDO){
                cancelNotification();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void cancelNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_NEWALERT);
    }

    private void sendNotification(String titulo,String mensaje,String tag,int alert_id,double lat,double lng,String direccion) {
        Intent intent = new Intent(this, HomeSeciActivity.class);
        intent.putExtra(S.GCM.from_push,true);
        intent.putExtra(S.GCM.message,mensaje);
        intent.putExtra(S.ALERTA.alert_id,alert_id);
        intent.putExtra(S.ALERTA.direccion,direccion);
        intent.putExtra(S.POSITION.latitude,lat);
        intent.putExtra(S.POSITION.longitude,lng);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_notification)
                .setContentTitle(titulo)
                .setContentText(mensaje)
                .setTicker(tag)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_NEWALERT /* ID of notification */, notificationBuilder.build());
    }
}