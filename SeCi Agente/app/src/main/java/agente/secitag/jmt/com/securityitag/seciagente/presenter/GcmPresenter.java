package agente.secitag.jmt.com.securityitag.seciagente.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.HashMap;
import java.util.Map;

import agente.secitag.jmt.com.securityitag.seciagente.model.API;
import agente.secitag.jmt.com.securityitag.seciagente.model.S;
import agente.secitag.jmt.com.securityitag.seciagente.model.data.JMStore;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.GcmView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class GcmPresenter {
    GcmView delegate;
    private String url;
    private Gson gson;
    private JMStore jmStore;
    public GcmPresenter(GcmView delegate){
        this.delegate = delegate;
        jmStore = new JMStore(delegate.getContext());
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }
    public void sendGcmId(){
        String gcm_id = getGcmId();
//        if(!jmStore.getBoolean(S.GCM.new_gcm_id)||gcm_id.equals(""))return;
//        if(gcm_id.equals(""))return;//TODO Send gcm_id constanly to server, hay error al actualizar el app
        if(gcm_id==null||gcm_id.equals(""))return;
        System.out.println("Sending gcm_id...!!!");
        url = S.VAR.base_url + "/api/user";
        Map<String,String> params = new HashMap<>();
        System.out.println("Enviando gcm->"+gcm_id);
        params.put("gcm_id",gcm_id);
        new API(delegate.getContext()).put(url, params);
        jmStore.putBoolean(S.GCM.new_gcm_id, false);
    }
    public void saveGcmId(String gcm_id){
        if(getGcmId().equals(gcm_id))return;
        System.out.println("Guardando gcm->"+gcm_id);
        jmStore.putBoolean(S.GCM.new_gcm_id, true);
        jmStore.putString(S.GCM.gcm_id, gcm_id);
    }
    public boolean hasGcmId(){
        return jmStore.getString(S.GCM.gcm_id)!=null&&jmStore.getString(S.GCM.gcm_id).length()>0;
    }
    public String getGcmId(){
        return jmStore.getString(S.GCM.gcm_id);
    }
}