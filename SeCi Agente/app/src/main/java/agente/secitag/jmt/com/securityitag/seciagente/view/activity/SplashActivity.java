package agente.secitag.jmt.com.securityitag.seciagente.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import agente.secitag.jmt.com.securityitag.seciagente.R;
import agente.secitag.jmt.com.securityitag.seciagente.presenter.GcmPresenter;
import agente.secitag.jmt.com.securityitag.seciagente.presenter.SplashPresenter;
import agente.secitag.jmt.com.securityitag.seciagente.service.gcm.GcmRegistrationIntentService;
import agente.secitag.jmt.com.securityitag.seciagente.view.fragment.SplashFragment;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.GcmView;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.SplashView;

public class SplashActivity extends AppCompatActivity implements SplashFragment.OnSplashListener,SplashView,GcmView{
    private final SplashFragment splashFragment = SplashFragment.instance();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private SplashPresenter presenter;
    private GcmPresenter gcmPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        presenter = new SplashPresenter(this);
        gcmPresenter = new GcmPresenter(this);
        showSplash();
    }

    private void showSplash(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container, splashFragment).commit();
    }

    @Override
    public void onSplashStarted() {
        if(!gcmPresenter.hasGcmId()){
            System.out.println("Revisando si puede pbtener GCM");
            RegistrationBroadcastReceiver();
        }else{
            System.out.println("Cargando datos de usuario");
            loadUserInfo();
        }
    }

    @Override
    public void onSplashStopped() {
    }

    private void RegistrationBroadcastReceiver() {
        if (checkPlayServices()) {
            loadUserInfo();
        }else{
            Toast.makeText(this,"Device does not support!",Toast.LENGTH_LONG).show();
        }
    }

    private void loadUserInfo() {
        Intent intent = new Intent(this, GcmRegistrationIntentService.class);
        startService(intent);
        presenter.getInfo();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                System.out.println("This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onRequestSuccess(Object object) {
        startActivity(new Intent(this,HomeSeciActivity.class));
        finish();
    }

    @Override
    public void onRequestError(Object object) {
        startActivity(new Intent(this,AccessPanelSeciActivity.class));
        finish();
    }
}