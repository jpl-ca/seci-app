package agente.secitag.jmt.com.securityitag.seciagente.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import agente.secitag.jmt.com.securityitag.seciagente.model.entity.UserE;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class UserAccessPresenter {
    private Gson gson;
    public UserAccessPresenter(){
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }
    public UserE getUserInfo(){
        List<UserE> users = UserE.listAll(UserE.class);
        return users.get(users.size()-1);
    }
}