package agente.secitag.jmt.com.securityitag.seciagente.view.interface_view;

import android.content.Context;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface MapAlertView {
    Context getContext();
    void onRequestIncidentSuccess(int type);
    void onRequestError(int error_type,String message);
}