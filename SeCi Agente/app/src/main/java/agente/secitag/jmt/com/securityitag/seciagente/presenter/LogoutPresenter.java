package agente.secitag.jmt.com.securityitag.seciagente.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import agente.secitag.jmt.com.securityitag.seciagente.model.API;
import agente.secitag.jmt.com.securityitag.seciagente.model.S;
import agente.secitag.jmt.com.securityitag.seciagente.model.data.JMStore;
import agente.secitag.jmt.com.securityitag.seciagente.model.entity.UserE;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.LogoutView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class LogoutPresenter {
    LogoutView delegate;
    private String url;
    private Gson gson;
    private JMStore jmStore;
    public LogoutPresenter(LogoutView delegate){
        this.delegate = delegate;
        jmStore = new JMStore(delegate.getContext());
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }
    public void logout(){
        url = S.VAR.base_url + "/api/oauth";
        new API(delegate.getContext()).delete(url);
        UserE.deleteAll(UserE.class);
    }
}