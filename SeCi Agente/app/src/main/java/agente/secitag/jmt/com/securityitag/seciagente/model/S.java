package agente.secitag.jmt.com.securityitag.seciagente.model;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public final class S {
    public static final class VAR{
        public static final String base_url = "http://45.55.217.27";
    }

    public static final class TAG{
        public static final String mac_address = "MAC_ADDRESS";
    }

    public static final class CLIENT{
        public static final String grant_type = "password";
        public static final String client_id = "seci_android";
        public static final String client_secret = "8lPZnWyF2KCOj148GIdWRzCov54eMa34J1SB0OD9";
        public static final String Authorization = "Authorization";
        public static final String access_token = "access_token";
        public static final String token_type = "token_type";
    }

    public static final class POSITION{
        public static final String latitude = "latitude";
        public static final String longitude = "longitude";
    }

    public static final class ALERTA{
        public static final String alert_id = "alert_id";
        public static final String direccion = "direccion";
        public static final int NUEVA_ALERTA = 5;
        public static final int ALERTA_FUE_ATENDIDO = 6;
        public static final int ATENCION_CORRECTO = 144;
        public static final int FINALIZACION_CORRECTO = 145;
        public static final int ERROR_ALERTA_FUE_ATENDIDO_PREVIAMENTE = 444;
        public static final int ERROR_NO_FINALIZADO = 445;
    }

    public static final class GCM{
        public static final String REGISTRATION_COMPLETE = "registrationComplete";
        public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
        public static final String SENDER_ID = "239948298742";
        public static final String gcm_id = "gcm_id";
        public static final String new_gcm_id = "new_gcm_id";
        public static final String date = "data";
        public static final String message = "message";
        public static final String from_push = "from_push";
    }
}