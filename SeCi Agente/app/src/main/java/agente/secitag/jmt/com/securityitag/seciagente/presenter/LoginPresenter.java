package agente.secitag.jmt.com.securityitag.seciagente.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import agente.secitag.jmt.com.securityitag.seciagente.R;
import agente.secitag.jmt.com.securityitag.seciagente.model.API;
import agente.secitag.jmt.com.securityitag.seciagente.model.S;
import agente.secitag.jmt.com.securityitag.seciagente.model.data.JMStore;
import agente.secitag.jmt.com.securityitag.seciagente.model.entity.UserE;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.ApiRequest;
import agente.secitag.jmt.com.securityitag.seciagente.view.interface_view.LoginView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class LoginPresenter {
    LoginView loginView;
    private String url;
    private Gson gson;
    private JMStore jmStore;
    private Context ctx;
    public LoginPresenter(LoginView loginView){
        this.loginView = loginView;
        ctx = loginView.getContext();
        jmStore = new JMStore(ctx);
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }

    public void login(final String username,final String password){
        url = S.VAR.base_url + "/api/oauth";
        Map<String,String> params = new HashMap<>();
        params.put("username",username);
        params.put("password",password);
        params.put("grant_type",S.CLIENT.grant_type);
        params.put("client_id",S.CLIENT.client_id);
        params.put("client_secret",S.CLIENT.client_secret);
        new API(loginView.getContext(),new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                try {
                    JSONObject job = new JSONObject(response.toString());
                    String access_token = job.getString("access_token");
                    String token_type = job.getString("token_type");
                    jmStore.putString(S.CLIENT.access_token, access_token);
                    jmStore.putString(S.CLIENT.token_type, token_type);
                    System.out.println(response.toString());
                    UserE.deleteAll(UserE.class);
                    UserE user = gson.fromJson(response.toString(), UserE.class);
                    user.save();
                    loginView.onRequestSuccess(response);
                } catch (JSONException e) {
                    loginView.onRequestError(response);
                }
            }

            @Override
            public void onRequestError(Object error) {
                JSONObject job = null;
                try {
                    job = new JSONObject(error.toString());
                    loginView.onRequestError(job.getString("error_description"));
                } catch (JSONException e) {
                    loginView.onRequestError(ctx.getString(R.string.s_error_conexion_servidor));
                }
            }
            @Override
            public void onRequestNull() {
//                login(username, password);
                loginView.onRequestError("Credenciales incorrectas");
            }
        }).post(url, params);
    }
}