package securityitag.com.jmt.secitag.seci.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.entity.UserE;
import securityitag.com.jmt.secitag.seci.model.API;
import securityitag.com.jmt.secitag.seci.view.interface_view.ApiRequest;
import securityitag.com.jmt.secitag.seci.view.interface_view.SplashView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class SplashPresenter {
    SplashView splashView;
    private String url;
    private Gson gson;
    public SplashPresenter(SplashView splashView){
        this.splashView = splashView;
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }
    public void getInfo(){
        url = S.VAR.base_url + "/api/user";
        new API(splashView.getContext(),new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                JSONObject job = null;
                try {
                    job = new JSONObject(response.toString());
                    String data = job.getString("data");
                    UserE.deleteAll(UserE.class);
                    UserE user = gson.fromJson(data, UserE.class);
                    user.save();
                    splashView.onRequestSuccess(response);
                } catch (JSONException e) {}
            }
            @Override
            public void onRequestError(Object error) {
                splashView.onRequestError(error);
            }

            @Override
            public void onRequestNull() {
                getInfo();
            }
        }).get(url);
    }
}