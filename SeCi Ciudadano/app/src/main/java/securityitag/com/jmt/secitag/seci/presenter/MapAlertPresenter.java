package securityitag.com.jmt.secitag.seci.presenter;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.API;
import securityitag.com.jmt.secitag.seci.model.entity.PlaceE;
//import securityitag.com.jmt.secitag.seci.service.API;
import securityitag.com.jmt.secitag.seci.view.interface_view.ApiRequest;
import securityitag.com.jmt.secitag.seci.view.interface_view.MapAlertView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class MapAlertPresenter {
    MapAlertView delegate;
    private String url;
    private Gson gson;
    private Context ctx;

    public MapAlertPresenter(MapAlertView delegate){
        this.delegate = delegate;
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
        ctx = delegate.getContext();
    }

    public void searchComSer(){
        url = S.VAR.base_url + "/api/milestone?q=milestone_type_id,=,1";
        searchPlaces(url);
    }
    public void searchHospClin(){
        url = S.VAR.base_url + "/api/milestone?q=milestone_type_id,=,2";
        searchPlaces(url);
    }
    public void searchPlaces(final String url){
        new API(delegate.getContext(),new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                try {
                    JSONObject job = new JSONObject(response.toString());
                    System.out.println("---->"+response.toString());
                    ArrayList<PlaceE> places = gson.fromJson(job.getString("data"), new TypeToken<ArrayList<PlaceE>>(){}.getType());
                    delegate.onRequestIncidentSuccess(places);
                }catch (JSONException e){
                    delegate.onRequestError(response);
                }
            }
            @Override
            public void onRequestError (Object error){
                delegate.onRequestError(error);
            }

            @Override
            public void onRequestNull() {
                searchPlaces(url);
            }
        }).get(url);
    }
    public void incidentEmergency(double lat,double lng){
        getLocationAddress(lat,lng);
    }
    public void incidentEmergency(final double lat,final double lng,final String address){
        url = S.VAR.base_url + "/api/incident-alert";
        Map<String,String> params = new HashMap<>();
        params.put("lat",String.valueOf(lat));
        params.put("lng",String.valueOf(lng));
        params.put("incident_alert_type_id",String.valueOf(1));
        params.put("address",address);
//        List<LocationE> locationEs = LocationE.listAll(LocationE.class);// Carga de posiciones registrados por el listener::::
        System.out.println("--->Alertando Desde:"+address);
        new API(delegate.getContext(),new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                try {
                    JSONObject job = new JSONObject(response.toString());
                    System.out.println("---->"+response.toString());
                }catch (JSONException e){
                    delegate.onRequestError(response);
                }
            }
            @Override
            public void onRequestError (Object error){
                delegate.onRequestError(error);
            }

            @Override
            public void onRequestNull() {
            }
        }).post(url, params);
    }

    public void getLocationAddress(final double orig_lat,final double orig_lon){
        url = "http://maps.google.com/maps/api/geocode/json?latlng="+orig_lat+","+orig_lon+"&sensor=false";
        new API(delegate.getContext(),new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                try {
                    HashSet<String> s = processJson(response.toString());
                    String address = "";
                    if(s.size()>0){
                        for (String add : s){
                            address = add;
                            break;
                        }
                    }
                    incidentEmergency(orig_lat, orig_lon,address);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onRequestError(Object error) {
                delegate.onRequestError(error);
            }
            @Override
            public void onRequestNull() {
                getLocationAddress(orig_lat,orig_lon);
            }
        }).get(url);
    }

    private HashSet<String> processJson(String s) throws JSONException {
        HashSet<String> addressLL = new HashSet<>();
        JSONObject job = new JSONObject(s);
        JSONArray jar = new JSONArray(job.getString("results"));
        for (int i=0; i<jar.length(); i++){
            JSONObject jo = jar.getJSONObject(i);
            String addressName = jo.getString("formatted_address");

            boolean ok = isTypeValid(jo.getJSONArray("types"));
            if(ok)addressLL.add(addressName.trim());

            JSONArray jarAddress = jo.getJSONArray("address_components");
            for (int j=0; j<jarAddress.length(); j++){
                JSONObject joAC = jarAddress.getJSONObject(j);
                addressName = joAC.getString("long_name");
                ok = isTypeValid(joAC.getJSONArray("types"));
                if(ok)addressLL.add(addressName.trim());
            }
        }
        return addressLL;
    }
    private boolean isTypeValid(JSONArray jo) throws JSONException {
        for (int j=0; j<jo.length(); j++){
            String str = jo.getString(j);
            if(str.equals("route")||str.equals("neighborhood"))return true;
        }
        return false;
    }
}