package securityitag.com.jmt.secitag.seci.view.interface_view;

import android.content.Context;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface LoginView{
    void onRequestSuccess(Object object);
    void onRequestError(int type,Object object);
    Context getContext();
}