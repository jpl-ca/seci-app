package securityitag.com.jmt.secitag.seci.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;
import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.entity.TagDeviceE;
import securityitag.com.jmt.secitag.seci.view.interface_view.RecyclerViewListener;

/**
 * Created by JMTech-Android on 27/10/2015.
 */
public class TagDeviceAdapter extends RecyclerView.Adapter<TagDeviceAdapter.ViewHolder> {
    private Context ctx;
    private ArrayList<TagDeviceE> data;
    private RecyclerViewListener rvListener;
    public TagDeviceAdapter(Context _ctx, ArrayList<TagDeviceE> data, RecyclerViewListener rvListener){
        ctx = _ctx;
        this.data = data;
        this.rvListener = rvListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_tag, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        TagDeviceE obj = data.get(position);
        holder.tv_tag_name.setText(obj.getName());
        holder.btn_conectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvListener.click(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_tag_name;
        private Button btn_conectar;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_tag_name = (TextView)itemView.findViewById(R.id.tv_tag_name);
            btn_conectar = (Button)itemView.findViewById(R.id.btn_conectar);
        }
    }
}