package securityitag.com.jmt.secitag.seci.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.HashMap;
import java.util.Map;

import securityitag.com.jmt.secitag.seci.model.API;
import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.data.JMStore;
//import securityitag.com.jmt.secitag.seci.service.API;
import securityitag.com.jmt.secitag.seci.view.interface_view.GcmView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class GcmPresenter {
    GcmView delegate;
    private String url;
    private Gson gson;
    private JMStore jmStore;
    public GcmPresenter(GcmView delegate){
        this.delegate = delegate;
        jmStore = new JMStore(delegate.getContext());
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }
    public void sendGcmId(){
        String gcm_id = getGcmId();
        if(gcm_id==null||gcm_id.equals(""))return;
        System.out.println("Sending gcm_id...!!!");
        url = S.VAR.base_url + "/api/user";
        Map<String,String> params = new HashMap<>();
        params.put("gcm_id",gcm_id);
        new API(delegate.getContext()).put(url, params);
        jmStore.putBoolean(S.GCM.new_gcm_id, false);
    }
    public void saveGcmId(String gcm_id){
        if(getGcmId().equals(gcm_id))return;
        jmStore.putBoolean(S.GCM.new_gcm_id, true);
        jmStore.putString(S.GCM.gcm_id, gcm_id);
    }
    public boolean hasGcmId(){
        return jmStore.getString(S.GCM.gcm_id)!=null&&jmStore.getString(S.GCM.gcm_id).length()>0;
    }
    public String getGcmId(){
        return jmStore.getString(S.GCM.gcm_id);
    }
}