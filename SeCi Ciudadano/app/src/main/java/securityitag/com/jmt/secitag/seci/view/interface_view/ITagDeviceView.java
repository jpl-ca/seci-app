package securityitag.com.jmt.secitag.seci.view.interface_view;

import android.content.Context;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface ITagDeviceView {
    Context getContext();
    void connectToDevice(String mac_address);
    void disconnectToDevice();
}