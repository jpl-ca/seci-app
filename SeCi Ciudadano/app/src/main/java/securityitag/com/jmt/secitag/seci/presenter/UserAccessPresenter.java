package securityitag.com.jmt.secitag.seci.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import securityitag.com.jmt.secitag.seci.model.API;
import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.data.JMStore;
import securityitag.com.jmt.secitag.seci.model.entity.UserE;
import securityitag.com.jmt.secitag.seci.view.interface_view.ApiRequest;
import securityitag.com.jmt.secitag.seci.view.interface_view.LogoutView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class UserAccessPresenter {
    private Gson gson;
    public UserAccessPresenter(){
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }
    public UserE getUserInfo(){
        List<UserE> users = UserE.listAll(UserE.class);
        return users.get(users.size()-1);
    }
}