package securityitag.com.jmt.secitag.seci.presenter;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import securityitag.com.jmt.secitag.seci.model.data.JMStore;
import securityitag.com.jmt.secitag.seci.model.entity.TagDeviceE;
import securityitag.com.jmt.secitag.seci.view.dialog.ListTagDialog;
import securityitag.com.jmt.secitag.seci.view.interface_view.ITagDeviceView;
import securityitag.com.jmt.secitag.seci.view.interface_view.ITagDialogView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class ITagDevicesPresenter implements ITagDialogView{
    ITagDeviceView delegate;
    private Gson gson;
    private Context ctx;
    private JMStore jmStore;

    public ITagDevicesPresenter(ITagDeviceView delegate){
        this.delegate = delegate;
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
        ctx = delegate.getContext();
        jmStore = new JMStore(ctx);
    }

    public void showRegisterIncidentDialog() {
        new ListTagDialog(ctx,this).show();
    }

    @Override
    public void onSelectDevice(TagDeviceE tag) {
        disconnectTag();
        tag.save();
        delegate.connectToDevice(tag.getMac_adreess());
    }

    @Override
    public List<TagDeviceE> getListTags() {
        List<TagDeviceE> tags = TagDeviceE.listAll(TagDeviceE.class);
        return tags;
    }

    @Override
    public void disconnectTag() {
        jmStore.setMacTagConnected("");
        delegate.disconnectToDevice();
    }

    @Override
    public void deleteTags() {
        TagDeviceE.deleteAll(TagDeviceE.class);
    }
}