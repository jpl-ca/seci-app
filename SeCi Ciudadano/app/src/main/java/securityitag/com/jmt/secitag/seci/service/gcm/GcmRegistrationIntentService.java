package securityitag.com.jmt.secitag.seci.service.gcm;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import java.io.IOException;
import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.data.JMStore;
import securityitag.com.jmt.secitag.seci.presenter.GcmPresenter;
import securityitag.com.jmt.secitag.seci.view.interface_view.GcmView;

public class GcmRegistrationIntentService extends IntentService implements GcmView{
    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    private GcmPresenter presenter;
    public GcmRegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        presenter = new GcmPresenter(this);
        String token = "";
        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            token = instanceID.getToken(S.GCM.SENDER_ID,GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            sendRegistrationToServer(token);
            subscribeTopics(token);
        } catch (Exception e) {
            System.out.println("Failed to complete token refresh");
        }
        Intent registrationComplete = new Intent(S.GCM.REGISTRATION_COMPLETE);
        registrationComplete.putExtra(S.GCM.gcm_id,token);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(String token) {
        System.out.println("GCM Registration Token: " + token);
        presenter.saveGcmId(token);
    }

    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }
}