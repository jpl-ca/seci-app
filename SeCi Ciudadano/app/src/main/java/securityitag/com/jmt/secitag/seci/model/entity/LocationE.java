package securityitag.com.jmt.secitag.seci.model.entity;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;
import java.io.Serializable;

/**
 * Created by JMTech-Android on 09/11/2015.
 */
public class LocationE extends SugarRecord<LocationE> implements Serializable {
    @Expose double Lat;
    @Expose double Lng;
    @Expose long time;

    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public double getLng() {
        return Lng;
    }
    public LatLng getLatLng() {
        return new LatLng(Lat,Lng);
    }

    public void setLng(double lng) {
        Lng = lng;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}