package securityitag.com.jmt.secitag.seci.view.interface_view;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface GpsView {
    Context getContext();
    void updateLatLng(LatLng ll);
}