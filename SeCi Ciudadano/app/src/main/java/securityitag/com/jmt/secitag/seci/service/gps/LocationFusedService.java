package securityitag.com.jmt.secitag.seci.service.gps;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import java.util.Date;

import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.entity.LocationE;

/**
 * Created by JMTech-Android on 09/06/2015.
 */
public class LocationFusedService extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 2*60*1000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation;
    public final static String NEW_GPS = "NEW_GPS";
    protected Boolean mRequestingLocationUpdates;
    Intent intent;

    @Override
    public void onCreate() {
        super.onCreate();
        mRequestingLocationUpdates = false;
        intent = new Intent();
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
            mRequestingLocationUpdates = true;
        }
        return super.onStartCommand(intent, flags, startId);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        intent.putExtra(S.POSITION.latitude,location.getLatitude());
        intent.putExtra(S.POSITION.longitude,location.getLongitude());
        intent.setAction(NEW_GPS);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        LocationE locationE = new LocationE();
        locationE.setLat(location.getLatitude());
        locationE.setLng(location.getLongitude());
        locationE.setTime(new Date().getTime());
        locationE.save();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onDestroy() {
        System.out.println("Finalizando");
        super.onDestroy();
        if (mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
        }
    }
}