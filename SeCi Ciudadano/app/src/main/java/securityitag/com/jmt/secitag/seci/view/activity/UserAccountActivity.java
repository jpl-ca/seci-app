package securityitag.com.jmt.secitag.seci.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.entity.UserE;
import securityitag.com.jmt.secitag.seci.presenter.LoginPresenter;
import securityitag.com.jmt.secitag.seci.presenter.UserAccessPresenter;
import securityitag.com.jmt.secitag.seci.view.fragment.LoginFragment;
import securityitag.com.jmt.secitag.seci.view.fragment.PanelFragment;
import securityitag.com.jmt.secitag.seci.view.fragment.RegisterFragment;
import securityitag.com.jmt.secitag.seci.view.fragment.UserAccountFragment;

/**
 * Created by JMTech-Android on 30/10/2015.
 */
public class UserAccountActivity extends AppCompatActivity implements UserAccountFragment.OnUserAccountListener{
    private final UserAccountFragment userFragment = UserAccountFragment.instance();
    private UserAccessPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_panel);
        presenter = new UserAccessPresenter();
        showPanel();
    }

    private void showPanel() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, userFragment).commit();
    }

    @Override
    public UserE getUserData() {
        UserE u = presenter.getUserInfo();
        return u;
    }
}