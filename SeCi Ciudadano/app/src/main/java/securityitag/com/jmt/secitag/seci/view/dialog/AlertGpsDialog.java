package securityitag.com.jmt.secitag.seci.view.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.view.interface_view.CallbackDialog;

/**
 * Created by JMTech-Android on 19/10/2015.
 */
public class AlertGpsDialog {
    private AlertDialog.Builder builder;
    private Context ctx;
    public AlertGpsDialog(Context ctx){
        this.ctx = ctx;
        builder = new AlertDialog.Builder(ctx, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(ctx.getString(R.string.seci_ciudadano));
    }
    public void show(final CallbackDialog _cb){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE: _cb.finish(); break;
                    case DialogInterface.BUTTON_NEGATIVE: break;
                }
            }
        };
        builder.setMessage(ctx.getString(R.string.s_activar_gps));
        builder.setNegativeButton(ctx.getString(R.string.cancelar), dialogClickListener);
        builder.setPositiveButton(ctx.getString(R.string.ok), dialogClickListener);
        builder.setCancelable(false);
        builder.show();
    }
}