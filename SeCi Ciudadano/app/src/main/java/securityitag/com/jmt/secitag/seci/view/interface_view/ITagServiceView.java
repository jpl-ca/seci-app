package securityitag.com.jmt.secitag.seci.view.interface_view;

import android.content.Context;

import securityitag.com.jmt.secitag.seci.model.S;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface ITagServiceView {
    Context getContext();
    void connectedTag(boolean b);
    void connectingTag();
    void oneClickTag();
}