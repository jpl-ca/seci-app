package securityitag.com.jmt.secitag.seci.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.entity.UserE;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class RegisterFragment extends Fragment{
    @Bind(R.id.txt_nombre_usuario) EditText txt_nombre_usuario;
    @Bind(R.id.txt_apellido_usuario) EditText txt_apellido_usuario;
    @Bind(R.id.txt_email) EditText txt_email;
    @Bind(R.id.txt_dni) EditText txt_dni;
    @Bind(R.id.txt_password) EditText txt_password;
    @Bind(R.id.sp_state) Spinner sp_state;
    @Bind(R.id.txt_phone) EditText txt_phone;
    @Bind(R.id.chck_acepto) CheckBox chck_acepto;
    @Bind(R.id.ll_root) LinearLayout ll_root;
    @Bind(R.id.ll_password) LinearLayout ll_password;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.btn_registrar) Button btn_registrar;
    @Bind(R.id.btn_cancelar) Button btn_cancel;
    private OnRegisterListener presenter;
    private UserE dataUser;
    public static RegisterFragment instance(){
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, rootView);
        toolbar.setTitle("  " + getString(R.string.s_crear_cuenta));
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        fillData();
    }

    private void fillData() {
        dataUser = presenter.getUserSocialNetwork();
        if(dataUser !=null){
            txt_email.setText(dataUser.getEmail());
            txt_nombre_usuario.setText(dataUser.getFirst_name());
            txt_apellido_usuario.setText(dataUser.getLast_name());
            txt_dni.setText("");
            ll_password.setVisibility(View.GONE);
            txt_email.setEnabled(false);
        }else{
            txt_email.setText("");
            txt_nombre_usuario.setText("");
            txt_apellido_usuario.setText("");
            txt_dni.setText("");
            txt_password.setText("");
            ll_password.setVisibility(View.VISIBLE);
            txt_email.setEnabled(true);
            txt_email.setEnabled(true);
        }
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        if (activity instanceof OnRegisterListener) {
            this.presenter = (OnRegisterListener) activity;
        } else {
            throw new ClassCastException("debe implementar OnRegisterListener");
        }
    }

    @OnClick(R.id.btn_registrar)
    public void Register(){
        if(!chck_acepto.isChecked()){
            showError(getString(R.string.debe_aceptar_terminos_condiciones));
            return;
        }
        if(dataUser==null)dataUser = new UserE();
        String nombre = txt_nombre_usuario.getText().toString();
        String apellidos = txt_apellido_usuario.getText().toString();
        String email = txt_email.getText().toString();
        String dni = txt_dni.getText().toString();
        String password = txt_password.getText().toString();
        dataUser.setFirst_name(nombre);
        dataUser.setLast_name(apellidos);
        dataUser.setId_number(dni);
        dataUser.setEmail(email);
        dataUser.setPassword(password);
        enableButtonRegister(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.register(dataUser);
            }
        }, 500);
    }

    @OnClick(R.id.btn_cancelar)
    public void back(){
        getActivity().onBackPressed();
    }

    public void enableButtonRegister(boolean b){
        btn_registrar.setEnabled(b);
        btn_cancel.setEnabled(b);
    }

    public void showError(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    public interface OnRegisterListener{
        void register(UserE user);
        UserE getUserSocialNetwork();
    }
}