package securityitag.com.jmt.secitag.seci.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import securityitag.com.jmt.secitag.seci.model.data.JMStore;
import securityitag.com.jmt.secitag.seci.service.bt.BluetoothTagService;

/**
 * Created by JMTech-Android on 25/11/2015.
 */
public class BootCompletedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent){
        if(new JMStore(context).isConnectedTagDevice()){
            System.out.println("Conectando automaticamente");
            context.startService(new Intent(context, BluetoothTagService.class));
        }else{
            System.out.println("Estuvo desconectado");
            context.sendBroadcast(new Intent(BluetoothTagService.ACTION_PREFIX + BluetoothTagService.NOTIFY_DISCONNECTED));
        }
    }
}