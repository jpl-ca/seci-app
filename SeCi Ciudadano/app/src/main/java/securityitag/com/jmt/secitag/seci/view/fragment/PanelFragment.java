package securityitag.com.jmt.secitag.seci.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.OnClick;
import butterknife.ButterKnife;
import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.entity.UserE;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class PanelFragment extends Fragment{
    @Bind(R.id.ll_root) LinearLayout ll_root;
    @Bind(R.id.btn_inicio_email)Button btn_inicio_email;
    @Bind(R.id.btn_register_email)Button btn_register_email;
    @Bind(R.id.ll_btn_sn) LinearLayout ll_btn_sn;
    @Bind(R.id.btnGp)Button btnGp;
    @Bind(R.id.btnFb)Button btnFb;

    private OnPanelListener presenter;
    private Context ctx;
    private CallbackManager mCallbackManager;
    private GoogleApiClient mGoogleApiClient;
    private boolean mResolvingError = false;
    private boolean FirstTime;
    private UserE dataUser;

    private ConnectionResult mConnectionResultGPlus;

    private static final int RC_SIGN_IN = 0;
    private static final int RC_SIGN_IN_FB = 64206;
    private boolean IS_GPLUS_CONNECTED = false;
    public static PanelFragment instance(){
        return new PanelFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_panel, container, false);
        ButterKnife.bind(this, rootView);
        ctx = getContext();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initFB();
        initGPlus();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void initGPlus() {
        FirstTime = true;
        mGoogleApiClient = new GoogleApiClient.Builder(ctx)
            .addApi(Plus.API)
            .addScope(new Scope("https://www.googleapis.com/auth/userinfo.email"))
            .addScope(new Scope("https://www.googleapis.com/auth/userinfo.profile"))
            .addScope(new Scope(Scopes.PLUS_LOGIN))
            .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(Bundle bundle) {
                    getProfileInformation();
                }

                @Override
                public void onConnectionSuspended(int i) {
                }
            })
            .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                @Override
                public void onConnectionFailed(ConnectionResult result) {
                    if (!result.hasResolution()) {
                        GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), getActivity(), 0).show();
                        return;
                    }
                    mConnectionResultGPlus = result;
                    if(FirstTime){
                        FirstTime = false;
                        signInWithGplus();
                    }
                }
            })
            .build();
    }

    private void initFB() {
        FacebookSdk.sdkInitialize(ctx);
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        if (activity instanceof OnPanelListener) {
            this.presenter = (OnPanelListener) activity;
        } else {
            throw new ClassCastException("debe implementar OnLoginListener");
        }
    }

    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            resolveSignInError();
        }
    }
    private void resolveSignInError() {
        if (mConnectionResultGPlus.hasResolution()) {
            try {
                mConnectionResultGPlus.startResolutionForResult(getActivity(), RC_SIGN_IN);
                checkConnectioGplus();
            } catch (IntentSender.SendIntentException e) {
                mGoogleApiClient.connect();
            }
        }
    }

    private void checkConnectioGplus() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                btnLoginGplus();
            }
        }, 7000);
    }

    @OnClick(R.id.btnGp)
    public void btnLoginGplus(){
        buttonsGone(true);
        dataUser = null;
        mGoogleApiClient.connect();
    }

    @OnClick(R.id.btnFb)
    public void btnLoginFacebook(){
        buttonsGone(true);
        dataUser = null;
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email","user_birthday"));
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            String id = object.getString("id");
                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
                            String email = object.getString("email");
                            dataUser = new UserE();
                            dataUser.setFirst_name(first_name);
                            dataUser.setLast_name(last_name);
                            dataUser.setEmail(email);
                            dataUser.setSocial_network(S.VAR.sn_facebook);
                            dataUser.setToken(loginResult.getAccessToken().getToken());
                            presenter.loginWithFacebook(loginResult.getAccessToken().getToken());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,birthday,link,email");
                request.setParameters(parameters);
                request.executeAsync();
            }
            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
                btnLoginFacebook();
            }
            @Override
            public void onError(FacebookException error) {}
        });
    }

    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                String id = currentPerson.getId()+"";
                String personName = currentPerson.getDisplayName();
                String personFirstName = currentPerson.getName().getGivenName();
                String personLastName = currentPerson.getName().getFamilyName();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                dataUser = new UserE();
                dataUser.setFirst_name(personFirstName);
                dataUser.setLast_name(personLastName);
                dataUser.setEmail(email);
                dataUser.setSocial_network(S.VAR.sn_google);
                IS_GPLUS_CONNECTED = true;
                getToken();
            } else {
                Toast.makeText(ctx, "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getToken(){
        final String account = Plus.AccountApi.getAccountName(mGoogleApiClient);
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String token = null;
                try {
//                    String mScope = "oauth2:"+Scopes.PLUS_LOGIN;
                    String mScope = "oauth2:profile email";
                    token = GoogleAuthUtil.getToken(ctx, account, mScope);
                } catch (UserRecoverableAuthException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (GoogleAuthException e) {
                    e.printStackTrace();
                }
                return token;
            }
            @Override
            protected void onPostExecute(String token) {
                System.out.println("TKN::>>" + token);
                dataUser.setToken(token);
                presenter.loginWithGPlus(token);

                //Cerrar Sesion
//                Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
//                mGoogleApiClient.disconnect();
            }
        };
        task.execute();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("El code de onActivityResult():" + requestCode + " - " + resultCode);//[GPLUS{64206 -1}]
        if (requestCode == RC_SIGN_IN) {
            System.out.println("To G+:");
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }else{
            System.out.println("To Fb:");
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R.id.btn_inicio_email)
    public void inicioSessionEmail(){
        presenter.loginWithEmail();
    }

    @OnClick(R.id.btn_register_email)
    public void registerEmail(){
        dataUser = null;
        presenter.registerWithEmail();
    }

    public UserE getDataUser() {
        return dataUser;
    }

    private void buttonsGone(boolean b) {
        if(b){
            btn_inicio_email.setVisibility(View.GONE);
            btn_register_email.setVisibility(View.GONE);
            ll_btn_sn.setVisibility(View.GONE);
        }else{
            btn_inicio_email.setVisibility(View.VISIBLE);
            btn_register_email.setVisibility(View.VISIBLE);
            ll_btn_sn.setVisibility(View.VISIBLE);
        }
    }

    public interface OnPanelListener{
        void loginWithEmail();
        void loginWithFacebook(String token);
        void loginWithGPlus(String token);
        void registerWithEmail();
    }
}