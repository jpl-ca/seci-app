package securityitag.com.jmt.secitag.seci.view.dialog;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.data.JMStore;
import securityitag.com.jmt.secitag.seci.model.entity.TagDeviceE;
import securityitag.com.jmt.secitag.seci.view.adapter.TagDeviceAdapter;
import securityitag.com.jmt.secitag.seci.view.interface_view.ITagDialogView;
import securityitag.com.jmt.secitag.seci.view.interface_view.RecyclerViewListener;

/**
 * Created by JMTech-Android on 19/10/2015.
 */
public class ListTagDialog{
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;
    private Context ctx;
    private ITagDialogView callback;
    private static final long SCAN_PERIOD = 5*1000; // 5 seconds

    private Handler mHandler;
    private BluetoothAdapter mBluetoothAdapter;
    private Runnable stopScan;
    private RecyclerView rv_list_tag;
    private RelativeLayout rl_current_tag;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView txt_name_tag_connected;
    private Button btn_buscar_dispositivos,btn_disconnect;
    private ArrayList<TagDeviceE> dataTags;
    private TagDeviceE currentTag;
    private final Map<String, String> devices = new HashMap<>();

    private static final List<String> DEFAULT_DEVICE_NAME = new ArrayList<>();
    static {
        DEFAULT_DEVICE_NAME.add("Quintic PROXR");
        DEFAULT_DEVICE_NAME.add("Cigii IT-02 Smart Bluetooth Tracker");
        DEFAULT_DEVICE_NAME.add("MLE-15");
        DEFAULT_DEVICE_NAME.add("MLE-16");
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback(){
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord){
            final String uuid = device.getAddress();
            final String name = device.getName();
            System.out.println(name+":::"+uuid+":::"+rssi);
            if (DEFAULT_DEVICE_NAME.contains(name)){
                devices.put((name == null) ? uuid : name, uuid);
            }
        }
    };

    public void scanTagStart(){
        stopScan = new Runnable() {
            @Override
            public void run(){
                setRefreshing(false);
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
                if (!devices.isEmpty()) {
                    displayListScannedDevices();
                }else{
                    showCurrentTag(false);
                }
            }
        };
        mHandler.postDelayed(stopScan, SCAN_PERIOD);
        mBluetoothAdapter.startLeScan(mLeScanCallback);
        setRefreshing(true);
    }

    public void scanTagStop(){
        mHandler.removeCallbacks(stopScan);
        mBluetoothAdapter.stopLeScan(mLeScanCallback);
    }
    public void showCurrentTag(boolean b){
        if(b){
            rl_current_tag.setVisibility(View.VISIBLE);
        }else{
            rl_current_tag.setVisibility(View.GONE);
        }
    }

    private void displayListScannedDevices() {
        Set<String> dev = devices.keySet();
        dataTags = new ArrayList<>();
        boolean showLastTag = false;
        for (String nm : dev){
            TagDeviceE tag = new TagDeviceE(nm, devices.get(nm));
            boolean same = false;
            if(currentTag!=null&&devices.get(nm).equals(currentTag.getMac_adreess())){
                same = showLastTag = true;
            }
            if(!same) dataTags.add(tag);
        }
        showCurrentTag(showLastTag);
        TagDeviceAdapter mAdapter = new TagDeviceAdapter(ctx, dataTags, new RecyclerViewListener() {
            @Override
            public void click(int position) {
                callback.deleteTags();
                tagSelected(dataTags.get(position));
            }
        });
        rv_list_tag.setAdapter(mAdapter);
        rv_list_tag.setItemAnimator(new DefaultItemAnimator());
    }

    public void tagSelected(TagDeviceE itag){
        callback.onSelectDevice(itag);
        alertDialog.dismiss();
    }

    public ListTagDialog(Context ctx,ITagDialogView callback){
        this.ctx = ctx;
        this.callback = callback;
        builder = new AlertDialog.Builder(ctx, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("    " + ctx.getString(R.string.s_lista_de_dispositivos).toUpperCase() + "    ");
        final BluetoothManager bluetoothManager = (BluetoothManager) ctx.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        mHandler = new Handler();
    }

    public void show(){
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_list_tag, null, false);
        rl_current_tag = (RelativeLayout)view.findViewById(R.id.rl_current_tag);
        txt_name_tag_connected = (TextView)view.findViewById(R.id.tv_tag_name);
        btn_disconnect = (Button)view.findViewById(R.id.btn_conectar);
        btn_buscar_dispositivos = (Button)view.findViewById(R.id.btn_scan);
        btn_disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btn_disconnect.getText().equals(ctx.getString(R.string.btn_conectar))){
                    if(currentTag!=null){
                        tagSelected(currentTag);
                        btn_disconnect.setText(ctx.getString(R.string.btn_desconectar));
                    }
                }else{
                    callback.disconnectTag();
                    waitToConnect();
                }
            }
        });
        btn_buscar_dispositivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanTagStart();
            }
        });
        searchCurrentTagInfo();
        rv_list_tag = (RecyclerView)view.findViewById(R.id.rv_list_tag);
        rv_list_tag.setLayoutManager(new LinearLayoutManager(ctx));
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_orange_dark, android.R.color.holo_green_dark, android.R.color.holo_red_dark, android.R.color.holo_blue_dark);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(){
                scanTagStart();
            }
        });
        builder.setPositiveButton(R.string.salir, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                scanTagStop();
            }
        });
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();
    }

    private void waitToConnect() {
        btn_disconnect.setEnabled(false);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                btn_disconnect.setEnabled(true);
                btn_disconnect.setText(ctx.getString(R.string.btn_conectar));
            }
        },1250);
    }

    private void searchCurrentTagInfo() {
        List<TagDeviceE> listaTag = callback.getListTags();
        if(listaTag.size()==0){
            showCurrentTag(false);
            return;
        }
        JMStore jmStore = new JMStore(ctx);
        System.out.println("---------------------------");
        System.out.println("El sz es:"+listaTag.size());
        for (TagDeviceE tg : listaTag){
            currentTag = tg;
            txt_name_tag_connected.setText(tg.getName());
            boolean same_mac = jmStore.lastMacTagConnected().equals(tg.getMac_adreess());
            if(same_mac&&jmStore.isConnectedTagDevice())
                 btn_disconnect.setText(ctx.getString(R.string.btn_desconectar));
            else btn_disconnect.setText(ctx.getString(R.string.btn_conectar));
            System.out.println(jmStore.lastMacTagConnected()+"|-----------|"+tg.getMac_adreess());
            System.out.println(same_mac+"---------------ISCONNECTED--->"+jmStore.isConnectedTagDevice());
        }
        System.out.println("---------------------------");
    }

    private void setRefreshing(final boolean refreshing){
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run(){
                mSwipeRefreshLayout.setRefreshing(refreshing);
            }
        });
    }
}