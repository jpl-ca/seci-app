package securityitag.com.jmt.secitag.seci.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import butterknife.Bind;
import butterknife.ButterKnife;
import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.data.JMStore;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class ResultGcmFragment extends Fragment{
    private OnResultGcmListener presenter;
    @Bind(R.id.txt_data) protected EditText txt_data;
    @Bind(R.id.txt_token) protected EditText txt_token;

    public static ResultGcmFragment instance(){
        return new ResultGcmFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_result_gcm, container, false);
        ButterKnife.bind(this, rootView);
        Bundle bundle = getActivity().getIntent().getExtras();
        String m = bundle.getString(S.GCM.message);
        String d = bundle.getString(S.GCM.date);
        System.out.println("*--------------------*");
        System.out.println(m);
        txt_token.setText(new JMStore(getContext()).getString(S.GCM.gcm_id));
        txt_data.setText(d);
        System.out.println("*--------------------*");
        return rootView;
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        if (activity instanceof OnResultGcmListener) {
            this.presenter = (OnResultGcmListener) activity;
        } else {
            throw new ClassCastException("debe implementar OnDashboardListener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        this.presenter.onResultGcmStarted();
    }

    @Override
    public void onStop(){
        super.onStop();
        this.presenter.onResultGcmStopped();
    }

    public interface OnResultGcmListener{
        void onResultGcmStarted();
        void onResultGcmStopped();
    }
}