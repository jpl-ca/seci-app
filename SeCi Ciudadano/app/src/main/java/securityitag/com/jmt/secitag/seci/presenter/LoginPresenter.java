package securityitag.com.jmt.secitag.seci.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.data.JMStore;
import securityitag.com.jmt.secitag.seci.model.entity.UserE;
//import securityitag.com.jmt.secitag.seci.service.API;
import securityitag.com.jmt.secitag.seci.model.API;
import securityitag.com.jmt.secitag.seci.view.interface_view.ApiRequest;
import securityitag.com.jmt.secitag.seci.view.interface_view.LoginView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class LoginPresenter {
    LoginView loginView;
    private String url;
    private Gson gson;
    private JMStore jmStore;
    public static int LOGIN_EMAIL_ERROR=10;
    public static int LOGIN_FB_ERROR=11;
    public static int LOGIN_GP_ERROR=12;
    public static int REGISTER_ERROR=15;
    public LoginPresenter(LoginView loginView){
        this.loginView = loginView;
        jmStore = new JMStore(loginView.getContext());
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }
    public void login(final String username,final String password){
        url = S.VAR.base_url + "/api/oauth";
        Map<String,String> params = new HashMap<>();
        params.put("username",username);
        params.put("password",password);
        params.put("grant_type",S.CLIENT.grant_type);
        params.put("client_id",S.CLIENT.client_id);
        params.put("client_secret",S.CLIENT.client_secret);
        new API(loginView.getContext(),new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                try {
                    JSONObject job = new JSONObject(response.toString());
                    String access_token = job.getString("access_token");
                    String token_type = job.getString("token_type");
                    jmStore.putString(S.CLIENT.access_token,access_token);
                    jmStore.putString(S.CLIENT.token_type, token_type);
                    System.out.println(response.toString());
                    UserE.deleteAll(UserE.class);
                    UserE user = gson.fromJson(response.toString(), UserE.class);
                    user.setSocial_network(null);
                    user.save();
                    loginView.onRequestSuccess(response);
                }catch (JSONException e){
                    loginView.onRequestError(LOGIN_EMAIL_ERROR,response);
                }
            }
            @Override
            public void onRequestError (Object error){
                JSONObject job = null;
                try {
                    job = new JSONObject(error.toString());
                    loginView.onRequestError(LOGIN_EMAIL_ERROR,job.getString("error_description"));
                } catch (JSONException e) {}
            }

            @Override
            public void onRequestNull() {
                login(username,password);
            }
        }).post(url, params);
    }
    public void loginSocial(final String typeLogin,final String social_access_token){
        url = S.VAR.base_url + "/api/oauth/social/"+typeLogin;
        Map<String,String> params = new HashMap<>();
        params.put("social_access_token",social_access_token);
        params.put("client_id",S.CLIENT.client_id);
        params.put("client_secret",S.CLIENT.client_secret);
        new API(loginView.getContext(),new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                try {
                    JSONObject job = new JSONObject(response.toString());
                    System.out.println("---------------->");
                    System.out.println(response.toString());
                    String access_token = job.getString("access_token");
                    String token_type = job.getString("token_type");
                    jmStore.putString(S.CLIENT.access_token,access_token);
                    jmStore.putString(S.CLIENT.token_type,token_type);
                    System.out.println(response.toString());
                    UserE.deleteAll(UserE.class);
                    UserE user = gson.fromJson(response.toString(), UserE.class);
                    user.setSocial_network(typeLogin);
                    user.save();
                    loginView.onRequestSuccess(response);
                }catch (JSONException e){
                    loginView.onRequestError(LOGIN_FB_ERROR,response);
                }
            }
            @Override
            public void onRequestError (Object error){
                if(typeLogin.equals(S.VAR.sn_facebook))
                     loginView.onRequestError(LOGIN_FB_ERROR, "");
                else loginView.onRequestError(LOGIN_GP_ERROR, "");
            }
            @Override
            public void onRequestNull() {
                if(typeLogin.equals(S.VAR.sn_facebook))
                    loginView.onRequestError(LOGIN_FB_ERROR, "");
                else loginView.onRequestError(LOGIN_GP_ERROR, "");
            }
        }).post(url, params);
    }

    public void register(final UserE user) {
        Map<String,String> params = new HashMap<>();
        params.put("first_name",user.getFirst_name());
        params.put("last_name",user.getLast_name());
        params.put("id_number",user.getId_number());
        params.put("email",user.getEmail());
        if(user.getSocial_network()==null){
            url = S.VAR.base_url + "/api/user";
            params.put("password",user.getPassword());
        }else{
            url = S.VAR.base_url + "/api/user";
            params.put("social_access_token",user.getToken());
            params.put("social_network",user.getSocial_network());
        }
        new API(loginView.getContext(),new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                String error_msg = "Error con el servidor";
                try {
                    JSONObject job = new JSONObject(response.toString());
                    System.out.println(response.toString());
                    if(job.has("message")){
                        if(user.getSocial_network()==null){
                            login(user.getId_number(),user.getPassword());
                        }else{
                            loginSocial(user.getSocial_network(),user.getToken());
                        }
                    }else{
                        System.out.println("Error p");
                        loginView.onRequestError(REGISTER_ERROR,error_msg);
                    }
                }catch (JSONException e){
                    loginView.onRequestError(REGISTER_ERROR,error_msg);
                }
            }
            @Override
            public void onRequestError (Object error){
                String error_msg = "Error con el servidor";
                try {
                    JSONObject job = new JSONObject(error.toString());
                    JSONObject jar = job.getJSONObject("errors");
                    Iterator<String> keys = jar.keys();
                    error_msg = "";
                    while (keys.hasNext()){
                        error_msg += jar.getString(keys.next())+"\n";
                    }
                    loginView.onRequestError(REGISTER_ERROR,error_msg);
                }catch (JSONException e){
                    loginView.onRequestError(REGISTER_ERROR,error_msg);
                }
            }
            @Override
            public void onRequestNull() {
            }
        }).post(url, params);
    }
}