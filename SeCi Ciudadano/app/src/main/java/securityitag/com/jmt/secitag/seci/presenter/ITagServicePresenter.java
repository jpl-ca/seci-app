package securityitag.com.jmt.secitag.seci.presenter;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.data.JMStore;
import securityitag.com.jmt.secitag.seci.service.bt.BluetoothTagService;
import securityitag.com.jmt.secitag.seci.view.interface_view.ITagServiceView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class ITagServicePresenter {
    ITagServiceView delegate;
    private Gson gson;
    private Context ctx;
    private String MAC_ADDRESS_BT = null;
    private BroadcastReceiver receiver;
    private final Handler handler;
    private JMStore jmStore;

    public ITagServicePresenter(ITagServiceView delegate){
        this.delegate = delegate;
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
        ctx = delegate.getContext();
        jmStore = new JMStore(ctx);
        handler = new Handler(ctx.getMainLooper());
        BluetoothAdapter.getDefaultAdapter().enable();
        serviceConnection();
    }

    public void startConnection(){
        LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, new IntentFilter(BluetoothTagService.GATT_CONNECTED));
        LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, new IntentFilter(BluetoothTagService.ONE_CLICK));
        LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, new IntentFilter(BluetoothTagService.GATT_DISCONNECTED));
        if(jmStore.lastMacTagConnected().equals("")){
            ctx.sendBroadcast(new Intent(BluetoothTagService.ACTION_PREFIX + BluetoothTagService.NOTIFY_DISCONNECTED));
            delegate.connectedTag(false);
        }else{
            MAC_ADDRESS_BT = jmStore.lastMacTagConnected();
            startTagService();
        }
    }

    public void tryTagConnection(String mac_address){
        MAC_ADDRESS_BT = mac_address;
        startTagService();
    }
    public void stopConnection(){
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver);
    }

    private void startTagService() {
        Intent it = new Intent(ctx, BluetoothTagService.class);
        it.putExtra(S.TAG.mac_address, MAC_ADDRESS_BT);
        ctx.startService(it);
        delegate.connectingTag();
    }

    public void stopServicesByLogout() {
        ctx.sendBroadcast(new Intent(BluetoothTagService.ACTION_PREFIX + BluetoothTagService.NOTIFY_CLOSE));
        stopTagService();
    }
    public void stopTagService() {
        ctx.stopService(new Intent(ctx, BluetoothTagService.class));
    }

    private void serviceConnection() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, final Intent intent){
                if (BluetoothTagService.GATT_CONNECTED.equals(intent.getAction())) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            delegate.connectedTag(true);
                        }
                    });
                }
                if (BluetoothTagService.ONE_CLICK.equals(intent.getAction())) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run(){
                            delegate.oneClickTag();
                        }
                    });
                }
                if (BluetoothTagService.GATT_DISCONNECTED.equals(intent.getAction())) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run(){
                            delegate.connectedTag(false);
//                            stopConnection();
                            stopTagService();
                        }
                    });
                }
            }
        };
    }

    private void runOnUiThread(Runnable r) {
        handler.post(r);
    }

}