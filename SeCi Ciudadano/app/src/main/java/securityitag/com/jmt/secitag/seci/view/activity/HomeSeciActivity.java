package securityitag.com.jmt.secitag.seci.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.entity.PlaceE;
import securityitag.com.jmt.secitag.seci.presenter.GcmPresenter;
import securityitag.com.jmt.secitag.seci.presenter.GpsPresenter;
import securityitag.com.jmt.secitag.seci.presenter.ITagDevicesPresenter;
import securityitag.com.jmt.secitag.seci.presenter.ITagServicePresenter;
import securityitag.com.jmt.secitag.seci.presenter.LogoutPresenter;
import securityitag.com.jmt.secitag.seci.presenter.MapAlertPresenter;
import securityitag.com.jmt.secitag.seci.service.bt.BluetoothTagService;
import securityitag.com.jmt.secitag.seci.view.dialog.AlertGpsDialog;
import securityitag.com.jmt.secitag.seci.view.fragment.MapHomeFragment;
import securityitag.com.jmt.secitag.seci.view.interface_view.CallbackDialog;
import securityitag.com.jmt.secitag.seci.view.interface_view.GcmView;
import securityitag.com.jmt.secitag.seci.view.interface_view.GpsView;
import securityitag.com.jmt.secitag.seci.view.interface_view.ITagDeviceView;
import securityitag.com.jmt.secitag.seci.view.interface_view.ITagServiceView;
import securityitag.com.jmt.secitag.seci.view.interface_view.LogoutView;
import securityitag.com.jmt.secitag.seci.view.interface_view.MapAlertView;

/**
 * Created by JMTech-Android on 30/10/2015.
 */
public class HomeSeciActivity extends AppCompatActivity implements MapHomeFragment.OnMapHomeListener,ITagServiceView,MapAlertView,GpsView,ITagDeviceView,LogoutView,GcmView{
    private ITagServicePresenter tagServicePresenter;
    private MapAlertPresenter homePresenter;
    private GpsPresenter gpsPresenter;
    private GcmPresenter gcmPresenter;
    private LogoutPresenter logoutPresenter;
    private ITagDevicesPresenter tagDevicesPresenter;
    private final MapHomeFragment mapHomeFragment = MapHomeFragment.instance();
    private int REQUEST_CODE = 10101;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        homePresenter = new MapAlertPresenter(this);
        tagServicePresenter = new ITagServicePresenter(this);
        gpsPresenter = new GpsPresenter(this);
        tagDevicesPresenter = new ITagDevicesPresenter(this);
        logoutPresenter = new LogoutPresenter(this);
        gcmPresenter = new GcmPresenter(this);
        showMapHome();
        startGeolocationGps();
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    private void showMapHome() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, mapHomeFragment).commit();
    }

    @Override
    public Context getContext() {
        return this;
    }

    ////Pertenece a ITagDevicesPresenter.
    @Override
    public void connectToDevice(String mac_address) {
        tagServicePresenter.tryTagConnection(mac_address);
    }
    @Override
    public void disconnectToDevice() {
        tagServicePresenter.stopTagService();
    }

    ////Pertenece a GPSView.
    @Override
    public void updateLatLng(LatLng ll) {
        mapHomeFragment.updateLatLng(ll);
    }

    ////Pertenece a ITagServiceView.
    @Override
    public void connectedTag(boolean b) {
        if(b) mapHomeFragment.connectedTag();
        else  mapHomeFragment.disconnectedTag();
    }
    @Override
    public void oneClickTag() {
        mapHomeFragment.clickTag();
    }
    @Override
    public void connectingTag() {
        mapHomeFragment.fabEnable(false);
    }

    ////Pertenece a MapHomeFragment.OnMapHomeListener.
    @Override
    public void onHomeMapStarted() {
        gpsPresenter.startConnection();
        gcmPresenter.sendGcmId();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            tagServicePresenter.startConnection();
    }
    @Override
    public void onHomeMapStopped() {
        gpsPresenter.stopConnection();
        tagServicePresenter.stopConnection();
    }
    @Override
    public void showListTagDialog() {
        tagDevicesPresenter.showRegisterIncidentDialog();
    }
    @Override
    public LatLng getLastPosition() {
        return gpsPresenter.getLastPosition();
    }

    @Override
    public void searchHospClin() {
        homePresenter.searchHospClin();
    }
    @Override
    public void searchComSer() {
        homePresenter.searchComSer();
    }

    @Override
    public void logout() {
        logoutPresenter.logout();
        Intent it = new Intent(this,AccessPanelSeciActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(it);
        finish();
        tagServicePresenter.stopServicesByLogout();
    }

    @Override
    public void showUserAccount() {
        Intent it = new Intent(this,UserAccountActivity.class);
        startActivity(it);
    }

    ////Pertenece a MapHomePresenter.
    @Override
    public void onRequestIncidentSuccess(ArrayList<PlaceE> places) {
        mapHomeFragment.showPlaces(places);
    }
    @Override
    public void onRequestError(Object object) {
    }

    // GPS ACTIVE
    private void startGeolocationGps() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if(!provider.contains("gps")){
            alertTurnGPS();
        }else{
            gpsPresenter.startGpsService();
        }
    }
    private void alertTurnGPS(){
        new AlertGpsDialog(this).show(new CallbackDialog() {
            @Override
            public void finish() {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_CODE && resultCode == 0){
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if(provider != null){
                startGeolocationGps();
            }else{
                alertTurnGPS();
            }
        }
    }
}