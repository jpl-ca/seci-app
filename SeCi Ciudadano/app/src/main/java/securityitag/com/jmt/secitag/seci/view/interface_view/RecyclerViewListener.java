package securityitag.com.jmt.secitag.seci.view.interface_view;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface RecyclerViewListener {
    void click(int position);
}