package securityitag.com.jmt.secitag.seci.service.bt;

import android.app.Service;
import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import java.sql.SQLOutput;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.data.JMStore;

/**
 * Created by JMTech-Android on 16/11/2015.
 */
public class BluetoothTagService extends Service {
    private String BT_MAC_ADDRESS = "";
    private JMStore jmStore;
    private Context ctx;
    private Handler mHandler;
    private BluetoothDevice mDevice;
    private BluetoothGatt bluetoothGatt;

    public static final String ACTION_PREFIX = "securityitag.com.jmt.action.";

    public static final String GATT_DISCONNECTED = "GATT_DISCONNECTED";
    public static final String GATT_CONNECTED = "GATT_CONNECTED";
    public static final String ONE_CLICK = "ONE_CLICK";
    public static final String CAPTURE_POSITION = "CAPTURE_POSITION";
    public static final String NOTIFY_CLOSE = "NOTIFY_CLOSE";
    public static final String NOTIFY_CONNECTED = "NOTIFY_CONNECTED";
    public static final String NOTIFY_DISCONNECTED = "NOTIFY_DISCONNECTED";
    public static final UUID FIND_ME_SERVICE = UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb");
    public static final UUID CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID FIND_ME_CHARACTERISTIC = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");

    private LocalBroadcastManager broadcaster;
    private static final long DELAY_DOUBLE_CLICK = 700;
    private long lastChange;
    private Runnable r;
    private Handler handler = new Handler();
    public BluetoothTagService() {
    }

    @Override
    public void onCreate() {
        System.out.println("-> onCreate called");
        this.broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ctx = this;
        jmStore = new JMStore(this);
        jmStore.setRunningTagService(true);
        System.out.println("-> onStartCommand Started");
        if(intent==null||!intent.hasExtra(S.TAG.mac_address)){
            if(BT_MAC_ADDRESS==null||BT_MAC_ADDRESS.equals("")){
                System.out.println("-> No hay mac address...!!!");
                sendBroadcast(new Intent(ACTION_PREFIX + NOTIFY_DISCONNECTED));
                stopSelf();
                return Service.START_NOT_STICKY;
            }
        }else{
            BT_MAC_ADDRESS = intent.getStringExtra(S.TAG.mac_address);
        }
        mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(BT_MAC_ADDRESS);
        connectToTabBT();
        return Service.START_STICKY;
    }

    private void connectToTabBT() {
        mHandler = new Handler(ctx.getMainLooper());
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if(bluetoothGatt!=null){
                    System.out.println("-> Ya esta conectado");
                    sendConnectedTag();
                }else{
                    bluetoothGatt = mDevice.connectGatt(ctx, false, btleGattCallback);
                }
            }
        });
    }

    private final BluetoothGattCallback btleGattCallback = new BluetoothGattCallback() {
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            byte[] data = characteristic.getValue();
            final String str = new String(data);
//            broadcaster.sendBroadcast(new Intent(ONE_CLICK));
            final long now = SystemClock.elapsedRealtime();
            if (lastChange + DELAY_DOUBLE_CLICK > now) {
                handler.removeCallbacks(r);
                r = new Runnable() {
                    @Override
                    public void run()
                    {
                        lastChange = 0;
                        System.out.println("DOOBLEE CLICK");
                        sendAlertMessage();
                    }
                };
                handler.postDelayed(r, DELAY_DOUBLE_CLICK);
            } else {
                lastChange = now;
                r = new Runnable() {
                    @Override
                    public void run(){
                        lastChange = 0;
                        System.out.println("UNNN CLICK");
                    }
                };
                handler.postDelayed(r, DELAY_DOUBLE_CLICK);
            }
        }

        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
            System.out.println("-> onConnectionStateChange() - STT:"+status+"|NEWST: "+newState);
            if (BluetoothGatt.GATT_SUCCESS == status) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    sendConnectedTag();
                    gatt.discoverServices();
                    System.out.println("-> Conectado AL TAG");
                    jmStore.setMacTagConnected(BT_MAC_ADDRESS);
                    jmStore.setConnectedTagDevice(true);
                    sendBroadcast(new Intent(ACTION_PREFIX + NOTIFY_CONNECTED));
                }else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    System.out.println("-> Desconectado del TAG");
                    broadcaster.sendBroadcast(new Intent(GATT_DISCONNECTED));
                    sendBroadcast(new Intent(ACTION_PREFIX + NOTIFY_DISCONNECTED));
                    new JMStore(ctx).setConnectedTagDevice(false);
                    stopSelf();
                }
            }else{
                if(status == 133){
//                    Toast.makeText(ctx,ctx.getString(R.string.s_error_connect_tag),Toast.LENGTH_LONG).show();
                    broadcaster.sendBroadcast(new Intent(GATT_DISCONNECTED));
                    sendBroadcast(new Intent(ACTION_PREFIX + NOTIFY_DISCONNECTED));
                }
                new JMStore(ctx).setConnectedTagDevice(false);
                if(bluetoothGatt!=null)
                    bluetoothGatt.close();
                bluetoothGatt = null;
                System.out.println("|-------------------- Hay un estado desconocido, al intentar conectar --------------------|");
            }
        }

        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
            enablePeerDeviceNotifyMe(gatt, true);
        }
    };

    private void sendAlertMessage() {
        sendBroadcast(new Intent(ACTION_PREFIX + CAPTURE_POSITION));
    }

    private void sendConnectedTag() {
        broadcaster.sendBroadcast(new Intent(GATT_CONNECTED));
    }

    public void enablePeerDeviceNotifyMe(BluetoothGatt bluetoothgatt, boolean flag){
        BluetoothGattCharacteristic bluetoothgattcharacteristic = getCharacteristic(bluetoothgatt, FIND_ME_SERVICE, FIND_ME_CHARACTERISTIC);
        if (bluetoothgattcharacteristic != null && (bluetoothgattcharacteristic.getProperties() | 0x10) > 0) {
            setCharacteristicNotification(bluetoothgatt, bluetoothgattcharacteristic, flag);
        }
    }
    private BluetoothGattCharacteristic getCharacteristic(BluetoothGatt bluetoothgatt, UUID serviceUuid, UUID characteristicUuid){
        if (bluetoothgatt != null) {
            BluetoothGattService service = bluetoothgatt.getService(serviceUuid);
            if (service != null)
                return service.getCharacteristic(characteristicUuid);
        }
        return null;
    }
    private void setCharacteristicNotification(BluetoothGatt bluetoothgatt, BluetoothGattCharacteristic bluetoothgattcharacteristic, boolean flag){
        bluetoothgatt.setCharacteristicNotification(bluetoothgattcharacteristic, flag);
        if (FIND_ME_CHARACTERISTIC.equals(bluetoothgattcharacteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = bluetoothgattcharacteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG);
            if (descriptor != null) {
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                bluetoothgatt.writeDescriptor(descriptor);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;// TODO: Return the communication channel to the service.
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        jmStore.setRunningTagService(false);
        if(bluetoothGatt!=null){
            bluetoothGatt.disconnect();
            bluetoothGatt.close();
            bluetoothGatt = null;
            jmStore.setConnectedTagDevice(false);
        }
        broadcaster.sendBroadcast(new Intent(GATT_DISCONNECTED));
    }
}