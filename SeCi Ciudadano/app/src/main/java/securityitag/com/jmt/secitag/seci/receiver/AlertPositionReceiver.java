package securityitag.com.jmt.secitag.seci.receiver;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

import java.util.ArrayList;

import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.entity.PlaceE;
import securityitag.com.jmt.secitag.seci.presenter.MapAlertPresenter;
import securityitag.com.jmt.secitag.seci.view.interface_view.MapAlertView;

/**
 * Created by JMTech-Android on 30/10/2015.
 */
public class AlertPositionReceiver extends BroadcastReceiver implements MapAlertView {
    static final Criteria criteria = new Criteria();
    static final long MAX_AGE = 60*1000; // 60 seconds
    private Context ctx;
    private MapAlertPresenter presenter;

    @Override
    public void onReceive(Context context, Intent intent) throws SecurityException{
        ctx = context;
        presenter = new MapAlertPresenter(this);
        // because some customers don't like Google Play Services…
        Location bestLocation = null;
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        for (final String provider : locationManager.getAllProviders()) {
            final Location location = locationManager.getLastKnownLocation(provider);
            final long now = System.currentTimeMillis();
            if (location != null
                    && (bestLocation == null || location.getTime() > bestLocation.getTime())
                    && location.getTime() > now - MAX_AGE) {
                bestLocation = location;
            }
        }

        if (bestLocation == null) {
            final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            locationManager.requestSingleUpdate(criteria, pendingIntent);
        }

        if (bestLocation != null) {
            final String position = bestLocation.getLatitude() + "," + bestLocation.getLongitude();
            System.out.println("------------------------------------------");
            System.out.println("LL::"+position);
            System.out.println("------------------------------------------");
            presenter.incidentEmergency(bestLocation.getLatitude(),bestLocation.getLongitude());
        }
    }

    @Override
    public Context getContext() {
        return ctx;
    }

    @Override
    public void onRequestIncidentSuccess(ArrayList<PlaceE> places) {}
    @Override
    public void onRequestError(Object object) {}
}