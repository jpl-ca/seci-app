package securityitag.com.jmt.secitag.seci.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.entity.UserE;
import securityitag.com.jmt.secitag.seci.presenter.LoginPresenter;
import securityitag.com.jmt.secitag.seci.view.fragment.LoginFragment;
import securityitag.com.jmt.secitag.seci.view.fragment.PanelFragment;
import securityitag.com.jmt.secitag.seci.view.fragment.RegisterFragment;
import securityitag.com.jmt.secitag.seci.view.interface_view.LoginView;

/**
 * Created by JMTech-Android on 30/10/2015.
 */
public class AccessPanelSeciActivity extends AppCompatActivity implements LoginFragment.OnLoginListener,LoginView,RegisterFragment.OnRegisterListener,PanelFragment.OnPanelListener{
    private final LoginFragment loginFragment = LoginFragment.instance();
    private final RegisterFragment registerFragment = RegisterFragment.instance();
    private final PanelFragment panelFragment = PanelFragment.instance();
    private LoginPresenter loginPresenter;
    private boolean isOnPanelFragment;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_panel);
        loginPresenter = new LoginPresenter(this);
        showPanel();
    }

    private void showPanel() {
        isOnPanelFragment = true;
        getSupportFragmentManager().beginTransaction().replace(R.id.container, panelFragment).commit();
    }

    private void showLogin() {
        isOnPanelFragment = false;
        getSupportFragmentManager().beginTransaction().replace(R.id.container, loginFragment).commit();
    }

    private void showRegister() {
        isOnPanelFragment = false;
        getSupportFragmentManager().beginTransaction().replace(R.id.container, registerFragment).commit();
    }

    @Override
    public void login(String user, String pass) {
        loginPresenter.login(user, pass);
    }

    @Override
    public void onRequestSuccess(Object object) {
        startActivity(new Intent(this,HomeSeciActivity.class));
        finish();
    }
    @Override
    public void onRequestError(int type,Object object) {
        if(type == LoginPresenter.LOGIN_EMAIL_ERROR){
            loginFragment.showError(object.toString());
            loginFragment.enableButtonLogin(true);
        }else if(type == LoginPresenter.LOGIN_FB_ERROR){
            showRegister();
        }else if(type == LoginPresenter.LOGIN_GP_ERROR){
            showRegister();
        }else if(type == LoginPresenter.REGISTER_ERROR){
            System.out.println("Error ai");
            registerFragment.enableButtonRegister(true);
            registerFragment.showError(object.toString());
        }
    }
    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void register(UserE user) {
        loginPresenter.register(user);
    }

    @Override
    public UserE getUserSocialNetwork() {
        return panelFragment.getDataUser();
    }

    @Override
    public void onBackPressed() {
        if(isOnPanelFragment){
            super.onBackPressed();
        }else{
            showPanel();
        }
    }

    @Override
    public void loginWithEmail() {
        showLogin();
    }

    @Override
    public void loginWithFacebook(String token) {
        loginPresenter.loginSocial(S.VAR.sn_facebook, token);
    }
    @Override
    public void loginWithGPlus(String token) {
        loginPresenter.loginSocial(S.VAR.sn_google,token);
    }

    @Override
    public void registerWithEmail() {
        showRegister();
    }
}