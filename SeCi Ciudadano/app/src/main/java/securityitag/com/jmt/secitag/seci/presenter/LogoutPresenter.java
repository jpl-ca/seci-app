package securityitag.com.jmt.secitag.seci.presenter;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import securityitag.com.jmt.secitag.seci.model.API;
import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.data.JMStore;
import securityitag.com.jmt.secitag.seci.model.entity.UserE;
//import securityitag.com.jmt.secitag.seci.service.API;
import securityitag.com.jmt.secitag.seci.view.interface_view.ApiRequest;
import securityitag.com.jmt.secitag.seci.view.interface_view.LogoutView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class LogoutPresenter {
    LogoutView delegate;
    private String url;
    private Gson gson;
    private JMStore jmStore;
    public LogoutPresenter(LogoutView delegate){
        this.delegate = delegate;
        jmStore = new JMStore(delegate.getContext());
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }
    public void logout(){
        url = S.VAR.base_url + "/api/oauth";
        new API(delegate.getContext(),new ApiRequest() {
            @Override
            public void onRequestSuccess(Object response) {
                System.out.println("goodithink:)");
            }
            @Override
            public void onRequestError (Object error){
                System.out.println("badithink:(");
            }

            @Override
            public void onRequestNull() {
                logout();
            }
        }).delete(url);
        UserE.deleteAll(UserE.class);
    }
}