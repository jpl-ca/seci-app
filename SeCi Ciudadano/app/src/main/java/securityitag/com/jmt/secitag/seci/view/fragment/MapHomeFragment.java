package securityitag.com.jmt.secitag.seci.view.fragment;

import android.animation.IntEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.entity.PlaceE;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class MapHomeFragment extends Fragment implements OnMapReadyCallback{
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.fabITag) protected FloatingActionButton fabITag;
    @Bind(R.id.fab_menu_busqueda) protected FloatingActionsMenu fab_menu_busqueda;
    private LatLng ME;
    private GoogleMap gMap;
    private Marker mrkr;
    private OnMapHomeListener presenter;
    private float MapZoom = 14;
    private CameraPosition cameraPosition;
    private Circle circleME;
    private ValueAnimator vAnimator;
    private boolean zoomEfect = false;
    private Marker mPlaces[];

    public static MapHomeFragment instance(){
        return new MapHomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map_home, container, false);
        ButterKnife.bind(this, rootView);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        toolbar.setLogo(R.mipmap.ic_logo_bar);
        toolbar.setTitle("  "+getString(R.string.s_tu_ubicacion));
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        return rootView;
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        if (activity instanceof OnMapHomeListener) {
            this.presenter = (OnMapHomeListener) activity;
        } else {
            throw new ClassCastException("debe implementar OnDashboardListener");
        }
    }

    @OnClick(R.id.fabITag)
    public void checkITag(){
        presenter.showListTagDialog();
    }

    @OnClick(R.id.fab_hosp_clin)
    public void BuscarHospitalClinica(){
        presenter.searchHospClin();
    }

    @OnClick(R.id.fab_com_ser)
    public void BuscarComisariaSerenazgo(){
        presenter.searchComSer();
    }

    @Override
    public void onStart(){
        super.onStart();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2)
            fabITag.setVisibility(View.VISIBLE);
        this.presenter.onHomeMapStarted();
    }

    @Override
    public void onStop(){
        super.onStop();
        this.presenter.onHomeMapStopped();
        if(circleME!=null) endEfectZoom();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        gMap = map;
        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        CameraUpdate cam = CameraUpdateFactory.newLatLngZoom(presenter.getLastPosition(),8);
        gMap.moveCamera(cam);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            }
        }, 5000);
        gMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                MapZoom = cameraPosition.zoom;
            }
        });
        updateLatLng(presenter.getLastPosition());
    }

    public void fabEnable(boolean b){
        fabITag.setEnabled(b);
        if(!b){
            fabITag.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(),R.color.flat_gray_light)));
            fabITag.setImageResource(R.mipmap.ic_unpaired);
        }
    }

    public void connectedTag(){
        ((Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(750);
        fabEnable(true);
        fabITag.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(),R.color.flat_green_ligth)));
        fabITag.setImageResource(R.mipmap.ic_paired);
    }
    public void disconnectedTag(){
        fabEnable(true);
        fabITag.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(),R.color.flat_gray_dark)));
        fabITag.setImageResource(R.mipmap.ic_unpaired);
        if(circleME!=null) endEfectZoom();
    }
    public void updateLatLng(LatLng ll){
        ME = ll;
        if(mrkr==null) mrkr = gMap.addMarker(new MarkerOptions().position(ME).title("Lima").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_blue)));
        else mrkr.setPosition(ME);
        cameraPosition = new CameraPosition.Builder().target(ME).zoom(MapZoom).build();
        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 5000, null);
    }

    public void clickTag(){
        if(!zoomEfect) startEfectZoom();
        else endEfectZoom();
        zoomEfect = !zoomEfect;
    }
    public void endEfectZoom(){
        circleME.remove();
        vAnimator.end();
    }
    public void startEfectZoom(){
        int strokeColor = ContextCompat.getColor(getContext(),R.color.flat_blue);
        circleME = gMap.addCircle(new CircleOptions().center(ME).strokeWidth(20).strokeColor(strokeColor).radius(100));
        vAnimator = new ValueAnimator();
        vAnimator.setRepeatCount(ValueAnimator.INFINITE);
        vAnimator.setRepeatMode(ValueAnimator.RESTART);
        vAnimator.setIntValues(0, 500);
        vAnimator.setDuration(1000);
        vAnimator.setEvaluator(new IntEvaluator());
        vAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        vAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedFraction = valueAnimator.getAnimatedFraction();
                circleME.setRadius(animatedFraction * 750);
            }
        });
        vAnimator.start();
    }

    public void showPlaces(ArrayList<PlaceE> places) {
        final LatLngBounds.Builder builderLL = new LatLngBounds.Builder();
        if(mPlaces!=null)
            for (int i=0;i<mPlaces.length;i++)mPlaces[i].remove();
        mPlaces = new Marker[places.size()];
        int idx = 0;
        builderLL.include(ME);
        for (PlaceE pl : places){
            BitmapDescriptor ic;
            if(pl.getMilestone_type_id()==1)
                ic = BitmapDescriptorFactory.fromResource(R.mipmap.ic_police_place);
            else
                ic = BitmapDescriptorFactory.fromResource(R.mipmap.ic_hospital_place);
            LatLng ll = new LatLng(pl.getLat(),pl.getLng());
            mPlaces[idx++]=gMap.addMarker(new MarkerOptions().position(ll).title(pl.getName()).icon(ic));
            builderLL.include(ll);
        }
        LatLngBounds bounds = builderLL.build();
        int padding = 150;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        gMap.animateCamera(cu);
    }

    public interface OnMapHomeListener{
        void onHomeMapStarted();
        void onHomeMapStopped();
        void showListTagDialog();
        LatLng getLastPosition();
        void searchHospClin();
        void searchComSer();
        void logout();
        void showUserAccount();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_map, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            presenter.logout();
            return true;
        }else if (id == R.id.action_profile) {
            presenter.showUserAccount();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}