package securityitag.com.jmt.secitag.seci.service.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.gcm.GcmListenerService;
import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.view.activity.SplashActivity;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class ListenerGcmService extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {
        System.out.println("From es::::"+from);
        System.out.println(data.toString());
        String message = data.getString("gcm.notification.message");
        if (from.startsWith("/topics/")) {
            System.out.println("message received from some topic.");
        } else {
            System.out.println("normal downstream message.");
        }
        sendNotification(message,data.toString());
    }
    private void sendNotification(String message,String data) {
//        Intent intent = new Intent(this, SplashActivity.class);
//        intent.putExtra(S.GCM.date,data);
//        intent.putExtra(S.GCM.message,message);
//        intent.putExtra(S.GCM.from_push,true);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);
//                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}