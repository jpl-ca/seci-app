package securityitag.com.jmt.secitag.seci.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.gson.Gson;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import securityitag.com.jmt.secitag.seci.R;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public class LoginFragment extends Fragment{
    @Bind(R.id.txt_usuario) AppCompatEditText txt_usuario;
    @Bind(R.id.txt_contrasena)AppCompatEditText txt_contrasena;
    @Bind(R.id.btn_ingresar)Button btn_register;
    @Bind(R.id.til_usuario) TextInputLayout til_usuario;
    @Bind(R.id.til_contrasena) TextInputLayout til_contrasena;
    @Bind(R.id.ll_root) LinearLayout ll_root;

    private OnLoginListener presenter;
    public static LoginFragment instance(){
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        if (activity instanceof OnLoginListener) {
            this.presenter = (OnLoginListener) activity;
        } else {
            throw new ClassCastException("debe implementar OnLoginListener");
        }
    }

    @OnClick(R.id.btn_ingresar)
    public void login(){
        enableButtonLogin(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.login(txt_usuario.getText().toString(), txt_contrasena.getText().toString());
            }
        }, 500);
    }

    public void enableButtonLogin(boolean b){
        btn_register.setEnabled(b);
    }

    public void showError(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    public interface OnLoginListener{
        void login(String user,String pass);
    }
}