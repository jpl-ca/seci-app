package securityitag.com.jmt.secitag.seci.model.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by JMTech-Android on 19/10/2015.
 */
public class JMStore {
    Context ctx;
    SharedPreferences DB;
    private String PREF_TAG_IS_CONNECTED = "IS_TAG_CONNECTED";
    private String PREF_IS_SERVICE_RUNNING = "IS_SERVICE_STARTED";
    private String PREF_MAC_BT = "PREF_MAC_BLUETOOTH";
    public JMStore(Context ctx){
        DB= ctx.getSharedPreferences("sp_seci_jm",Context.MODE_PRIVATE);
    }
    public void putString(String key,String value) {
        System.out.println("Guardando:"+value);
        SharedPreferences.Editor editor = DB.edit();
        editor.putString(key,value);
        editor.apply();
    }
    public void removeString(String key) {
        SharedPreferences.Editor editor = DB.edit();
        editor.remove(key);
        editor.apply();
    }
    public String getString(String key) {
        if(DB.contains(key)) {
            return DB.getString(key, "");
        }
        return "";
    }

    public void setConnectedTagDevice(boolean running) {
        SharedPreferences.Editor editor = DB.edit();
        editor.putBoolean(PREF_TAG_IS_CONNECTED, running);
        editor.apply();
    }

    public boolean isConnectedTagDevice() {
        return DB.getBoolean(PREF_TAG_IS_CONNECTED, false);
    }

    public void setRunningTagService(boolean running) {
        SharedPreferences.Editor editor = DB.edit();
        editor.putBoolean(PREF_IS_SERVICE_RUNNING, running);
        editor.apply();
    }

    public boolean isRunningTagService() {
        return DB.getBoolean(PREF_IS_SERVICE_RUNNING, false);
    }

    public void setMacTagConnected(String mac_address) {
        putString(PREF_MAC_BT,mac_address);
    }

    public String lastMacTagConnected() {
        return getString(PREF_MAC_BT);
    }

    public void putBoolean(String key,boolean value) {
        System.out.println("Guardando:"+value);
        SharedPreferences.Editor editor = DB.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }
    public boolean getBoolean(String key) {
        if(DB.contains(key)) {
            return DB.getBoolean(key, false);
        }
        return false;
    }
}