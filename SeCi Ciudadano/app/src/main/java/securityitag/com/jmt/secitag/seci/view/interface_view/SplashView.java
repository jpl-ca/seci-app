package securityitag.com.jmt.secitag.seci.view.interface_view;

import android.content.Context;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface SplashView{
    Context getContext();
    void onRequestSuccess(Object object);
    void onRequestError(Object object);
}