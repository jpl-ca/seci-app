package securityitag.com.jmt.secitag.seci.view.interface_view;

import java.util.List;
import securityitag.com.jmt.secitag.seci.model.entity.TagDeviceE;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface ITagDialogView {
    void onSelectDevice(TagDeviceE tag);
    List<TagDeviceE> getListTags();
    void disconnectTag();
    void deleteTags();
}