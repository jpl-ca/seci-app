package securityitag.com.jmt.secitag.seci.receiver;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import securityitag.com.jmt.secitag.seci.model.data.JMStore;

/**
 * Created by JMTech-Android on 30/10/2015.
 */
public class StateBluetoothTagReceiver extends BroadcastReceiver {
    private LocalBroadcastManager broadcaster;
    @Override
    public void onReceive(Context context, Intent intent){
        broadcaster = LocalBroadcastManager.getInstance(context);
        System.out.println("-> BT:"+intent.getAction());
        if (intent.getAction().equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
            System.out.println("-> BT Conectado");
        }else if (intent.getAction().equals(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)) {
            System.out.println("-> BT DesConectando...");
        }else if (intent.getAction().equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)) {
            System.out.println("-> BT Desconectado");
            //TODO Creo que ya no es necesario esto.
//            new JMStore(context).setConnectedTagDevice(false);// Registrar que se ha desconectado
        }else{
            final int bluetoothState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
            System.out.println("BT:---->bluetooth change state: " + bluetoothState);
            if (bluetoothState == BluetoothAdapter.STATE_ON) {}else{}
        }
    }
}