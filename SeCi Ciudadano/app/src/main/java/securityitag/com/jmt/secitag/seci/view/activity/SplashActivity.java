package securityitag.com.jmt.secitag.seci.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.presenter.GcmPresenter;
import securityitag.com.jmt.secitag.seci.presenter.SplashPresenter;
import securityitag.com.jmt.secitag.seci.service.gcm.GcmRegistrationIntentService;
import securityitag.com.jmt.secitag.seci.view.fragment.ResultGcmFragment;
import securityitag.com.jmt.secitag.seci.view.fragment.SplashFragment;
import securityitag.com.jmt.secitag.seci.view.interface_view.GcmView;
import securityitag.com.jmt.secitag.seci.view.interface_view.SplashView;

public class SplashActivity extends AppCompatActivity implements SplashFragment.OnSplashListener,SplashView,ResultGcmFragment.OnResultGcmListener,GcmView{
    private final SplashFragment splashFragment = SplashFragment.instance();
    private final ResultGcmFragment resultFragment = ResultGcmFragment.instance();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private SplashPresenter presenter;
    private GcmPresenter gcmPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        presenter = new SplashPresenter(this);
        gcmPresenter = new GcmPresenter(this);
        Bundle extras = getIntent().getExtras();
        if(extras!=null&&extras.containsKey(S.GCM.from_push)){
            showResultGCM();
        }else{
            showSplash();
        }
    }

    private void showSplash(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container, splashFragment).commit();
    }

    private void showResultGCM(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container, resultFragment).commit();
    }

    @Override
    public void onSplashStarted() {
        if(!gcmPresenter.hasGcmId()){
            System.out.println("Revisando si puede pbtener GCM");
            RegistrationBroadcastReceiver();
        }else{
            System.out.println("Cargando datos de usuario");
            loadUserInfo();
        }
    }

    @Override
    public void onSplashStopped() {
    }

    private void RegistrationBroadcastReceiver() {
        if (checkPlayServices()) {
            loadUserInfo();
        }else{
            Toast.makeText(this, "Device does not support!", Toast.LENGTH_LONG).show();
        }
    }

    private void loadUserInfo() {
        Intent intent = new Intent(this, GcmRegistrationIntentService.class);
        startService(intent);
        presenter.getInfo();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                System.out.println("This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onResultGcmStarted() {
    }
    @Override
    public void onResultGcmStopped() {
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onRequestSuccess(Object object) {
        startActivity(new Intent(this,HomeSeciActivity.class));
        finish();
    }

    @Override
    public void onRequestError(Object object) {
        startActivity(new Intent(this,AccessPanelSeciActivity.class));
        finish();
    }
}