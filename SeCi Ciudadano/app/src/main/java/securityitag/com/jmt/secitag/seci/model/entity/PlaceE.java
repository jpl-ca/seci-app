package securityitag.com.jmt.secitag.seci.model.entity;

import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by JMTech-Android on 09/11/2015.
 */
public class PlaceE extends SugarRecord<PlaceE> implements Serializable {
    @Expose String name;
    @Expose double lat;
    @Expose double lng;
    @Expose int milestone_type_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getMilestone_type_id() {
        return milestone_type_id;
    }

    public void setMilestone_type_id(int milestone_type_id) {
        this.milestone_type_id = milestone_type_id;
    }
}