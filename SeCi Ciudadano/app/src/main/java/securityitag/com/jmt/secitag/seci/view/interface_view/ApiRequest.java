package securityitag.com.jmt.secitag.seci.view.interface_view;

import org.json.JSONException;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface ApiRequest {
    void onRequestSuccess(Object object);
    void onRequestError(Object object);
    void onRequestNull();
}