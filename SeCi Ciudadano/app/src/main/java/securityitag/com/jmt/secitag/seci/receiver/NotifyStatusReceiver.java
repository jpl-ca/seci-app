package securityitag.com.jmt.secitag.seci.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

import com.google.gson.Gson;

import java.util.Date;

import securityitag.com.jmt.secitag.seci.R;
import securityitag.com.jmt.secitag.seci.model.data.JMStore;
import securityitag.com.jmt.secitag.seci.service.bt.BluetoothTagService;
import securityitag.com.jmt.secitag.seci.view.activity.HomeSeciActivity;
import securityitag.com.jmt.secitag.seci.view.fragment.MapHomeFragment;

/**
 * Created by JMTech-Android on 25/11/2015.
 */
public class NotifyStatusReceiver extends BroadcastReceiver {
    private Context ctx;
    private int NOTIFICATION_ID = 202;
    private NotificationManager mNotificationManager;
    @Override
    public void onReceive(Context context, Intent intent){
        ctx = context;
        if(intent.getAction().equals(BluetoothTagService.ACTION_PREFIX+BluetoothTagService.NOTIFY_CLOSE)){
            cancelNotification();
        }else if(intent.getAction().equals(BluetoothTagService.ACTION_PREFIX+BluetoothTagService.NOTIFY_CONNECTED)){
            cancelNotification();
        }else if(intent.getAction().equals(BluetoothTagService.ACTION_PREFIX+BluetoothTagService.NOTIFY_DISCONNECTED)){
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                showNotification();
        }
    }

    private void cancelNotification() {
        mNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(NOTIFICATION_ID);
    }

    private void showNotification() {
        String str = ctx.getString(R.string.s_dispositivo_desconectado_al_celular);
        Intent intent = new Intent(ctx, HomeSeciActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(ctx)
                .setSmallIcon(R.mipmap.ic_notify)
                .setContentTitle(ctx.getString(R.string.app_name))
                .setContentText(str)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = notificationBuilder.build();
        notification.flags = Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_ONGOING_EVENT;
        notificationManager.notify(NOTIFICATION_ID /* ID of notification */, notification);

        long[] pattern = {100,100,150,100,150,100,150};
        ((Vibrator)ctx.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(pattern, -1);
    }
}