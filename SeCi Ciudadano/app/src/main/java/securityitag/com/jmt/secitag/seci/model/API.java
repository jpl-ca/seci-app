package securityitag.com.jmt.secitag.seci.model;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import securityitag.com.jmt.secitag.seci.model.data.JMStore;
import securityitag.com.jmt.secitag.seci.view.ApplicationController;
import securityitag.com.jmt.secitag.seci.view.interface_view.ApiRequest;

/**
 * Created by JMTech-Android on 26/11/2015.
 */
public class API implements Response.Listener<String>,Response.ErrorListener{
    private Context ctx;
    private ApiRequest callback;
    private JMStore jmStore;
    private Map<String, String> headers;

    public API(Context ctx, ApiRequest callback){
        this.ctx = ctx;
        this.callback = callback;
        headers = new HashMap<>();
        jmStore = new JMStore(ctx);
    }
    public API(Context ctx){
        this.ctx = ctx;
        headers = new HashMap<>();
        jmStore = new JMStore(ctx);
    }

    public void get(String service_url){
        System.out.println("http-get:"+service_url);
        exec(service_url,Request.Method.GET,new HashMap<String, String>());
    }
    public void delete(String service_url){
        System.out.println("http-delete:"+service_url);
        exec(service_url,Request.Method.DELETE,new HashMap<String, String>());
    }
    public void post(String service_url,Map<String,String> params){
        System.out.println("http-post:"+service_url);
        exec(service_url,Request.Method.POST,params);
    }
    public void put(String service_url,Map<String,String> params){
        System.out.println("http-put:"+service_url);
        exec(service_url,Request.Method.PUT,params);
    }

    public void exec(String service_url,int request_method,final Map<String,String> params){
        StringRequest stringRequest = new StringRequest(request_method,service_url, this, this){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                loadHeaderSaved(headers);
                return headers;
            }
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }
            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError){
                if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                }
                return volleyError;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(stringRequest);
    }

    private void loadHeaderSaved(Map<String, String> headers) {
        headers.put(S.CLIENT.Authorization,jmStore.getString(S.CLIENT.token_type)+" "+jmStore.getString(S.CLIENT.access_token));
        System.out.println("CK="+S.CLIENT.Authorization+":"+jmStore.getString(S.CLIENT.token_type)+" "+jmStore.getString(S.CLIENT.access_token));
    }

    @Override
    public void onResponse(String response) {
        //Comentado
//        System.out.println("->"+response);
        if (callback!=null) callback.onRequestSuccess(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if(error.getMessage()!=null)
        System.out.println("E:->"+error.getMessage());
        if(error.getMessage()==null){
            System.out.println("Retorno error NULL");
            callback.onRequestNull();
        }else{
            System.out.println("Error:::"+error.getMessage());
            if (callback!=null) callback.onRequestError(error.getMessage());
        }
    }
}