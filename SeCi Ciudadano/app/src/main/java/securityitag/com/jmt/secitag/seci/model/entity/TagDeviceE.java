package securityitag.com.jmt.secitag.seci.model.entity;

import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;
import java.io.Serializable;

/**
 * Created by JMTech-Android on 09/11/2015.
 */
public class TagDeviceE extends SugarRecord<TagDeviceE> implements Serializable {
    @Expose String name;
    @Expose String mac_adreess;
    @Expose boolean connected;

    public TagDeviceE(){}

    public TagDeviceE(String name,String mac_adreess){
        this.name = name;
        this.mac_adreess = mac_adreess;
        this.connected = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMac_adreess() {
        return mac_adreess;
    }

    public void setMac_adreess(String mac_adreess) {
        this.mac_adreess = mac_adreess;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}