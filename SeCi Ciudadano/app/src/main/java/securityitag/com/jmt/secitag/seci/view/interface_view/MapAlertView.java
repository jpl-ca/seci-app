package securityitag.com.jmt.secitag.seci.view.interface_view;

import android.content.Context;

import java.util.ArrayList;

import securityitag.com.jmt.secitag.seci.model.S;
import securityitag.com.jmt.secitag.seci.model.entity.PlaceE;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface MapAlertView {
    Context getContext();
    void onRequestIncidentSuccess(ArrayList<PlaceE> places);
    void onRequestError(Object object);
}