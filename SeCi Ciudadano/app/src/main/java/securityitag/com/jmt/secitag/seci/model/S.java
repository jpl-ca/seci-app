package securityitag.com.jmt.secitag.seci.model;

/**
 * Created by JMTech-Android on 05/11/2015.
 */
public final class S {
    public static final class VAR{
        public static final String base_url = "http://45.55.217.27";
        public static String sn_facebook = "facebook";
        public static String sn_google = "google";
        public static String ClientIdGPlus = "239948298742-hnr85vl0gau2rqe8o2srthi6bgopq549.apps.googleusercontent.com";
    }

    public static final class TAG{
        public static final String mac_address = "MAC_ADDRESS";
    }

    public static final class CLIENT{
        public static final String client_secret = "8lPZnWyF2KCOj148GIdWRzCov54eMa34J1SB0OD9";
        public static final String client_id = "seci_android";
        public static final String grant_type = "password";
        public static final String Authorization = "Authorization";
        public static final String access_token = "access_token";
        public static final String token_type = "token_type";
    }

    public static final class POSITION{
        public static final String latitude = "latitude";
        public static final String longitude = "longitude";
    }

    public static final class GCM{
        public static final String REGISTRATION_COMPLETE = "registrationComplete";
        public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
        public static final String SENDER_ID = "239948298742";
        public static final String gcm_id = "gcm_id";
        public static final String date = "data";
        public static final String message = "message";
        public static final String from_push = "from_push";
        public static String new_gcm_id = "new_gcm_id";
    }
}